clear ; close all;clc

G_Results_FolderName=horzcat('testResults'); mkdir(G_Results_FolderName);
G_Dataset_FolderName=horzcat('toyDatasets');
G_PlotFunctions_FolderName=horzcat('functions');

addpath(G_Dataset_FolderName);
addpath(G_Results_FolderName);

load TestList.mat
G_TestList=TestList; clear TestList
G_earlystopCriteria_Minutes=15;
    
for G_iter_test=1:length(G_TestList)
    %% ID
    Global.ID=G_TestList(G_iter_test).ID;    
    fprintf('\n   ID: '); disp(G_TestList(G_iter_test).ID); fprintf('G_iter_test %d\n ',G_iter_test)    
    %% SPHEREFLAG
    Global.SPHEREFLAG=G_TestList(G_iter_test).sphereflag;
    %% classification
    Global.LAMBDA=G_TestList(G_iter_test).lambda;
    minute_seconds=60;
    Global.TIMEOUT=G_earlystopCriteria_Minutes*minute_seconds;
    if strcmp( G_TestList(G_iter_test).classifier,'none')
        Global.MinMax=false;
        Global.MinMaxMode=G_TestList(G_iter_test).classifier; % 'Parc' 'Arc' 'Gparc'
    else
        Global.MinMax=true;
        Global.MinMaxMode=G_TestList(G_iter_test).classifier; % 'Parc' 'Arc' 'Gparc'
    end
    %% CLUSTERING
    Global.EPSILON = G_TestList(G_iter_test).epsilon;
    Global.CLUSTER_ARRAY={G_TestList(G_iter_test).clustering_interval};
    Global.CLUSTERING = G_TestList(G_iter_test).kmeans;    %'partitional'; ... Set the clustering mode: 'partitional', 'agglomerative', 'divisive'
    Global.DISTANCE = G_TestList(G_iter_test).clustering_metric; % Set the distance to be used in clustering algorithm: 'euclidean', 'sqEuclidean', 'manhattan', 'mahalanobis' or function handle
    Global.REPRESENTATIVE = G_TestList(G_iter_test).clustering_representative; % Set the representative type: 'centroid', 'medoid', 'median'
    Global.DEFUZZIFICATION=G_TestList(G_iter_test).defuzzification; % 'WTA' or 'AVG'  '; % 'WTA' or 'AVG'    
    Global.REPLICATE=G_TestList(G_iter_test).numberOfReplicates;
    Global.ITERATION=G_TestList(G_iter_test).numberOfIteration;
    %% PRINT
    Global.printFlag=true;
    if Global.printFlag==true
        addpath(G_PlotFunctions_FolderName);
    end    
    %% PARALLEL
    Global.parallelFlag=false;
    if Global.printFlag==true && G_TestList(G_iter_test).numberOfInput~=2 && Global.parallelFlag==true && length(Global.CLUSTER_ARRAY)~=1
        if  G_TestList(G_iter_test).numeroInput~=2
                error('input dimension must be equal to 2');
        end
        if  Global.parallelFlag==true
                error('deactivate parfor');
        end 
        if length(Global.CLUSTER_ARRAY)~=1
            error('Global.CLUSTER_ARRAY must be rediuced to one dimension');
        end
        error('error printflag');
    end
    %% CARICA DATASET
    Global.NAMEDATASET= G_TestList(G_iter_test).nameDataset;
    load(horzcat(G_Dataset_FolderName , filesep , Global.NAMEDATASET,'.mat'));    
    %% RUN ANFIS TOOLBOX
    try
        tic;
        [fis_model] = execute_ANFIS_tool(Global,TrainingSet,TrainingLabels,ValidationSet,ValidationLabels);
        time=toc;
    catch ER
        G_TestList(G_iter_test).PERFORMANCE = ER.message;
        fis_model.trainedFlag = false;
    end
    %% PRINT
    if Global.printFlag==true && fis_model.trainedFlag==true
        plotMembership(fis_model, [TrainingSet; ValidationSet; TestSet]);
        FontSize=30;
        % Load saved figures
        c=  figure(1);
        k=  figure(2);%figure(fis_model.fis.k);
        % Prepare subplots
        figure2print=figure('units','normalized','outerposition',[0 0 1 1]);
        h(1)=subplot(1,2,1);         
        % Paste figures on the subplots
        copyobj(allchild(get(c,'CurrentAxes')),h(1));
        
        axis square; grid on;box on;
        titleplot=title('MinMax Hyperboxes');
        xlab=xlabel('$x$');
        ylab=ylabel('$y$');
        set(xlab,'Interpreter','latex','FontSize',FontSize);
        set(ylab,'Interpreter','latex','FontSize',FontSize);
        set(titleplot,'Interpreter','latex','FontSize',FontSize);
        h(2)=subplot(1,2,2); 
        copyobj(allchild(get(k,'CurrentAxes')),h(2));
        axis square; grid on; box on;
        titleplot=title('Membership Functions');
        xlab=xlabel('$x$');
        ylab=ylabel('$y$');
        set(xlab,'Interpreter','latex','FontSize',FontSize);
        set(ylab,'Interpreter','latex','FontSize',FontSize);
        set(titleplot,'Interpreter','latex','FontSize',FontSize);
        % salva
        figureName='figure';
        pathFile=fullfile(pwd,filesep,G_Results_FolderName,[figureName '.fig']);  
        saveas(figure2print,pathFile);
        close all
    end
    %% SAVE RESULTS AND EVALUATE THE PERFORMANCES
    if fis_model.trainedFlag == true
        solution=fis_model;
        fis_model=fis_model.fis;
        % performance evaluation
        TestLabels_predicted=fis_model.predict(TestSet);
        RMSE.TS= ( sum( (TestLabels_predicted-TestLabels).^2) /length(TestLabels) )^0.5;
        RMSE.VL= fis_model.rmse;
        Rsquared.TS = rsquared_eval(TestLabels,TestLabels_predicted);        
        % SAVE
        G_TestList(G_iter_test).solution=solution;
        G_TestList(G_iter_test).solution.list=[];
        G_TestList(G_iter_test).PERFORMANCE.timeEffort=time;
        G_TestList(G_iter_test).PERFORMANCE.RMSE=RMSE;
        G_TestList(G_iter_test).PERFORMANCE.Rsquared=Rsquared.TS;        
        G_TestList(G_iter_test).PERFORMANCE.ANFIS=fis_model;
        G_TestList(G_iter_test).PERFORMANCE.DBI = fis_model.DBI;
        G_TestList(G_iter_test).PERFORMANCE.SILHOUETTE = fis_model.silhouette;        
    end
    clearvars -except G_*

    pathFile_test=fullfile(horzcat(filesep,G_Results_FolderName), horzcat('results.mat'));
    save([pwd pathFile_test ],'G_TestList');
end

clearvars -except TestList