function outputArg = fitAgglomerative(obj,Xtr,Ytr,K)

%%% Regularisation term for badly-conditioned matrices
% global reg;
% reg = 10^(-7);

% strip flag for parallel computing
if obj.parallel == false
    parforArg = 0;
else
    parforArg = Inf;
end

[numPatterns, numFeatures] = size(Xtr);
deltaRMSeval = 'diff';
for thisK = K:-1:1
    % eval bottom layer thanks to partitional
    outputArg = obj.fitPartitional(Xtr,Ytr,thisK);
    if sum(cell2mat(outputArg{1}))~=0
        break
    else
        warning('CLUST:LowK', '\tLowering initial K at %d due to failed clustering.',thisK-1);
        pause(1);
    end
end
% if no solutions at all
if thisK==1 && sum(cell2mat(outputArg{1}))==0
    error('The Agglomerative procedure failed to find an initial layer.');
else
    [CENTROIDS,MEMBERSHIPS,HYPERPLANES,COVARIANCES,CARDINALITIES,ERROR,CLUSTER_PATTERNS,CLUSTER_LABELS,INVCOVARIANCES,MAXANFISMF,FEASIBILITY] = deal(cell(K,K));
    %K = thisK;
end

% set bottom level (actually, the first: building upside-down)
tic; fprintf('\tBuilding level 1...');
startLvl = K-thisK+1;
CENTROIDS(startLvl,1:thisK)=outputArg{2};
MEMBERSHIPS(startLvl,1:thisK)=outputArg{3};
HYPERPLANES(startLvl,1:thisK)=outputArg{4};
CARDINALITIES(startLvl,1:thisK)=outputArg{6};
CLUSTER_PATTERNS(startLvl,1:thisK)=cellfun(@(x) Xtr(x,:), MEMBERSHIPS(startLvl,1:thisK),'UniformOutput',false);
CLUSTER_LABELS(startLvl,1:thisK)=cellfun(@(x) Ytr(x,:), MEMBERSHIPS(startLvl,1:thisK),'UniformOutput',false);
for i=1:thisK
    ERROR{startLvl,i} = sum((CLUSTER_LABELS{startLvl,i}-([ones(size(CLUSTER_PATTERNS{startLvl,i},1),1) CLUSTER_PATTERNS{startLvl,i}]*HYPERPLANES{startLvl,i})).^2);
end
fprintf('done!\t\t\t[Time elapsed: %.2f]\n',toc);
% build other layers
for lvl=startLvl+1:K
    tic; fprintf('\tBuilding level %d...',lvl);
    % evaluate distance between representatives
    representatives = reshape(cell2mat(CENTROIDS(lvl-1,:)),numFeatures,[])';
    thisNumClusters = size(representatives,1);
    switch obj.inputDistance
        case {'euclidean', 'mahalanobis'}
            D = pdist2(representatives,representatives,'euclidean');
            D = D/sqrt(numFeatures);
        case 'sqEuclidean'
            D = pdist2(representatives,representatives,'euclidean').^2;
            D = D/sqrt(numFeatures);
        case 'manhattan'
            D = pdist2(representatives,representatives,'cityblock');
            D = D/numFeatures;
        otherwise
            D = zeros(size(representatives,1));
            for p=1:size(representatives,1)
                for q=1:size(representatives,1)
                    D(p,q) = obj.distance(representatives(p,:),representatives(q,:));
                end
            end
    end
    % evaluate RMS distances
    deltaRMS = zeros(thisNumClusters);
    for i=1:thisNumClusters
        for j=1:thisNumClusters
            joinedLabels = [CLUSTER_LABELS{lvl-1,i} ; CLUSTER_LABELS{lvl-1,j}];
            joinedPatterns = [CLUSTER_PATTERNS{lvl-1,i} ; CLUSTER_PATTERNS{lvl-1,j}];
            singleErrors = mean([ERROR{lvl-1,i}, ERROR{lvl-1,j}]);
            joinedHyperplane = pinv([ones(size(joinedPatterns,1),1) joinedPatterns])*joinedLabels;
            joinedError = sum((joinedLabels-([ones(size(joinedPatterns,1),1) joinedPatterns]*joinedHyperplane)).^2);
            switch deltaRMSeval
                case 'diff'
                    deltaRMS(i,j) = 2 * (joinedError - singleErrors);
                case 'onlyJoin'
                    deltaRMS(i,j) = joinedError;
            end
        end
    end
    % place inf in main diagonal to trick the min operator
    deltaRMS = deltaRMS + diag(ones(1,thisNumClusters)*inf);
    D = D + diag(ones(1,thisNumClusters)*inf);
    % score matrix
    S = max(D,deltaRMS);    % S = (D + deltaRMS)/2;
    % cluster to fuse
    [r,c] = find(S==min(S(:)));
    cluster1 = r(1);
    cluster2 = c(1);
    % move clusters not-to-be-fused to next level
    goodClusters = setdiff(1:thisNumClusters,[cluster1 cluster2]);
    CLUSTER_PATTERNS(lvl,2:2+length(goodClusters)-1) = CLUSTER_PATTERNS(lvl-1,goodClusters);
    CLUSTER_LABELS(lvl,2:2+length(goodClusters)-1) = CLUSTER_LABELS(lvl-1,goodClusters);
    CARDINALITIES(lvl,2:2+length(goodClusters)-1) = CARDINALITIES(lvl-1,goodClusters);
    CENTROIDS(lvl,2:2+length(goodClusters)-1) = CENTROIDS(lvl-1,goodClusters);
    HYPERPLANES(lvl,2:2+length(goodClusters)-1) = HYPERPLANES(lvl-1,goodClusters);
    ERROR(lvl,2:2+length(goodClusters)-1) = ERROR(lvl-1,goodClusters);
    MEMBERSHIPS(lvl,2:2+length(goodClusters)-1) = MEMBERSHIPS(lvl-1,goodClusters);
    % fuse!
    CLUSTER_PATTERNS{lvl,1} = [CLUSTER_PATTERNS{lvl-1,cluster1} ; CLUSTER_PATTERNS{lvl-1,cluster2}];
    CLUSTER_LABELS{lvl,1} = [CLUSTER_LABELS{lvl-1,cluster1} ; CLUSTER_LABELS{lvl-1,cluster2}];
    CARDINALITIES{lvl,1} = CARDINALITIES{lvl-1,cluster1} + CARDINALITIES{lvl-1,cluster2};
    switch obj.representative
        case 'medoid'
            switch obj.inputDistance
                case 'euclidean'
                    pairwiseDistanceMatrix = pdist2(CLUSTER_PATTERNS{lvl,1},CLUSTER_PATTERNS{lvl,1},'euclidean');
                case 'mahalanobis'
                    try
                        pairwiseDistanceMatrix = pdist2(CLUSTER_PATTERNS{lvl,1},CLUSTER_PATTERNS{lvl,1},'mahalanobis', nancov(CLUSTER_PATTERNS{lvl,1}));
                    catch
                        try
                            pairwiseDistanceMatrix = pdist2(CLUSTER_PATTERNS{lvl,1},CLUSTER_PATTERNS{lvl,1},'mahalanobis', fixCovarianceMatrix(nancov(CLUSTER_PATTERNS{lvl,1})));
                        catch % if singleton
                            pairwiseDistanceMatrix = pdist2(CLUSTER_PATTERNS{lvl,1},CLUSTER_PATTERNS{lvl,1},'euclidean');
                        end
                    end
                case 'sqEuclidean'
                    pairwiseDistanceMatrix = pdist2(CLUSTER_PATTERNS{lvl,1},CLUSTER_PATTERNS{lvl,1},'euclidean').^2;
                case 'manhattan'
                    pairwiseDistanceMatrix = pdist2(CLUSTER_PATTERNS{lvl,1},CLUSTER_PATTERNS{lvl,1},'cityblock');
                otherwise
                    pairwiseDistanceMatrix = zeros(size(CLUSTER_PATTERNS{lvl,1},1));
                    for p=1:size(CLUSTER_PATTERNS{lvl,1},1)
                        for q=1:size(CLUSTER_PATTERNS{lvl,1},1)
                            pairwiseDistanceMatrix(p,q) = obj.distance(CLUSTER_PATTERNS{lvl,1}(p,:),CLUSTER_PATTERNS{lvl,1}(q,:));
                        end
                    end
            end
            [~,medoidID] = min(sum(pairwiseDistanceMatrix,1));
            CENTROIDS{lvl,1} = CLUSTER_PATTERNS{lvl,1}(medoidID,:);
        case 'centroid'
            CENTROIDS{lvl,1} = mean(CLUSTER_PATTERNS{lvl,1},1);
        case 'median'
            CENTROIDS{lvl,1} = median(CLUSTER_PATTERNS{lvl,1},1);
    end
    %
    MEMBERSHIPS{lvl,1} = [MEMBERSHIPS{lvl-1,cluster1} ; MEMBERSHIPS{lvl-1,cluster2}];
    HYPERPLANES{lvl,1} = pinv([ones(size(CLUSTER_PATTERNS{lvl,1},1),1) CLUSTER_PATTERNS{lvl,1}])*CLUSTER_LABELS{lvl,1};
    ERROR{lvl,1} = sum((CLUSTER_LABELS{lvl,1}-([ones(size(CLUSTER_PATTERNS{lvl,1},1),1) CLUSTER_PATTERNS{lvl,1}]*HYPERPLANES{lvl,1})).^2);
    fprintf('done!\t\t\t[Time elapsed: %.2f]\n',toc);
end

% flip the three
COVARIANCES = flipud(COVARIANCES);
INVCOVARIANCES = flipud(INVCOVARIANCES);
CENTROIDS = flipud(CENTROIDS);
MEMBERSHIPS = flipud(MEMBERSHIPS);
ERROR = flipud(ERROR);
CLUSTER_PATTERNS = flipud(CLUSTER_PATTERNS);
CLUSTER_LABELS = flipud(CLUSTER_LABELS);
CARDINALITIES = flipud(CARDINALITIES);
MAXANFISMF = flipud(MAXANFISMF);
HYPERPLANES = flipud(HYPERPLANES);

% batch evaluation of (inverse) covariance matrices + max ANFIS
fullCells=cellfun(@(x) ~isempty(x),CLUSTER_PATTERNS);
COVARIANCES(fullCells) = cellfun(@(x) cov(x),CLUSTER_PATTERNS(fullCells),'UniformOutput',false);
COVARIANCES = reshape(COVARIANCES,K,K);
MAXANFISMF(fullCells) = cellfun(@(x) (2*pi)^(-numFeatures/2) * det(x)^(-1/2), COVARIANCES(fullCells), 'UniformOutput',false);
MAXANFISMF = reshape(MAXANFISMF,K,K);
FEASIBILITY(fullCells) = cellfun(@(x) x>numFeatures ,CARDINALITIES(fullCells),'UniformOutput',false);

unfiasibleLvl = all(fullCells==0);
FEASIBILITY(unfiasibleLvl,:) = {0};
FEASIBILITY = reshape(FEASIBILITY,K,K);

%%% Davies-Bouldin Index
if obj.DaviesBouldin == true
    DBI = zeros(K,1);
    tic; fprintf('\nEvaluating Davies-Bouldin Index...');
    for k = 2:K
        s = zeros(1,k);
        Rij = zeros(k);
        % evaluating pairwise distances between clusters (representatives)
        representatives = CENTROIDS(k,cellfun(@(x) ~isempty(x),CENTROIDS(k,:)));
        representatives = reshape(cell2mat(representatives),numFeatures,[])';
        %         switch algorithm
        %             case 'kmeans'
        %                 d = pdist2(representatives_asmatrix,representatives_asmatrix,'euclidean').^2;
        %             case 'kmedians'
        %                 d = pdist2(representatives_asmatrix,representatives_asmatrix,'cityblock');
        %             case 'kmedoids'
        %                 d = pdist2(representatives_asmatrix,representatives_asmatrix,'euclidean');
        %         end
        switch obj.inputDistance
            case {'euclidean', 'mahalanobis'}
                d = pdist2(representatives,representatives,'euclidean');
            case 'sqEuclidean'
                d = pdist2(representatives,representatives,'euclidean').^2;
            case 'manhattan'
                d = pdist2(representatives,representatives,'cityblock');
            otherwise
                d = zeros(size(representatives,1));
                for p=1:size(representatives,1)
                    for q=1:size(representatives,1)
                        d(p,q) = obj.distance(representatives(p,:),representatives(q,:));
                    end
                end
        end
        % evaluating intra-cluster average distance
        for i=1:k
            %             patternsInCluster = CLUSTER_PATTERNS{k,i};
            %             switch algorithm
            %                 case 'kmeans'
            %                     s(i) = mean(pdist2(patternsInCluster,representatives{i},'euclidean').^2);
            %                 case 'kmedians'
            %                     s(i) = mean(pdist2(patternsInCluster,representatives{i},'cityblock'));
            %                 case 'kmedoids'
            %                     s(i) = mean(pdist2(patternsInCluster,representatives{i},'mahalanobis',COVARIANCES{k,i}));
            %             end
            switch obj.inputDistance
                case 'euclidean'
                    s(i) = mean(pdist2(CLUSTER_PATTERNS{k,i},representatives(i,:),'euclidean'));
                case 'mahalanobis'
                    s(i) = mean(pdist2(CLUSTER_PATTERNS{k,i},representatives(i,:),'mahalanobis',COVARIANCES{k,i}));
                case 'sqEuclidean'
                    s(i) = mean(pdist2(CLUSTER_PATTERNS{k,i},representatives(i,:),'euclidean').^2);
                case 'manhattan'
                    s(i) = mean(pdist2(CLUSTER_PATTERNS{k,i},representatives(i,:),'cityblock'));
                otherwise
                    tmp = zeros(size(CLUSTER_PATTERNS{k,i},1),1);
                    for p=1:length(tmp)
                        tmp(p) = obj.distance(representatives(i,:),CLUSTER_PATTERNS{k,i}(p,:));
                    end
                    s(i) = mean(tmp);
            end
        end
        % evaluating cross-variance between clusters
        for i=1:k
            for j=1:k
                Rij(i,j)=(s(i)+s(j))/(d(i,j));
            end
        end
        Rij(1:k+1:end) = -1; % place -1 on diagonal to trick the max operator
        R = max(Rij,[],2);
        DBI(k) = sum(R)/k;
    end
    DBI(1) = NaN;   % DBI is not defined for k = 1 cluster
    fprintf('done!\t\t\t[Time elapsed: %.2f]\n',toc);
elseif obj.DaviesBouldin == false
    DBI = NaN(K,1); % NaN as a dummy value if user does not want DBI
end

%%% Silhouette
if obj.Silhouette == true
    fprintf('Evaluating the Silhouette...'); tic;
    SILHOUETTE = NaN(K,1);
    for k = 2:K
        [a,b,s]=deal(zeros(numPatterns,1));
        parfor(p=1:numPatterns,parforArg) % can be done in parallel
            clusterID = cellfun(@(x) find(x==p),MEMBERSHIPS(k,:),'UniformOutput',false);
            clusterID = find(cellfun(@(x) ~isempty(x),clusterID));
            clusterMembersID = MEMBERSHIPS{k,clusterID};

            %         a_tmp = zeros(length(clusterMembersID),1);
            %         for i = 1:length(clusterMembersID)
            %             a_tmp(i) = obj.joinedDistance(Xtr(p,:), Xtr(clusterMembersID(i),:), ) %pattern,label,centroid,hyperplane,cluster,epsilon
            %         end

            switch obj.inputDistance
                case 'euclidean'
                    a_tmp = pdist2(Xtr(p,:),Xtr(clusterMembersID,:),'euclidean');
                case 'mahalanobis'
                    a_tmp = pdist2(Xtr(p,:),Xtr(clusterMembersID,:),'mahalanobis',COVARIANCES{k,clusterID});
                case 'sqEuclidean'
                    a_tmp = pdist2(Xtr(p,:),Xtr(clusterMembersID,:),'euclidean').^2;
                case 'manhattan'
                    a_tmp = pdist2(Xtr(p,:),Xtr(clusterMembersID,:),'cityblock');
                otherwise
                    a_tmp = zeros(length(clusterMembersID),1);
                    for q=1:length(a_tmp)
                        a_tmp(q) = obj.distance(Xtr(p,:),Xtr(clusterMembersID(q),:));
                    end
            end
            a(p) = sum(a_tmp) / (length(clusterMembersID)-1);

            b_tmp = [];
            for i = 1:k
                if i~=clusterID
                    notClusterMembersID = MEMBERSHIPS{i};
                    switch obj.inputDistance
                        case 'euclidean'
                            b_tmp = [b_tmp mean(pdist2(Xtr(p,:),Xtr(notClusterMembersID,:),'euclidean'))];
                        case 'mahalanobis'
                            b_tmp = [b_tmp mean(pdist2(Xtr(p,:),Xtr(notClusterMembersID,:),'mahalanobis',COVARIANCES{k,i}))];
                        case 'sqEuclidean'
                            b_tmp = [b_tmp mean(pdist2(Xtr(p,:),Xtr(notClusterMembersID,:),'euclidean').^2)];
                        case 'manhattan'
                            b_tmp = [b_tmp mean(pdist2(Xtr(p,:),Xtr(notClusterMembersID,:),'cityblock'))];
                        otherwise
                            tmp = zeros(length(notClusterMembersID),1);
                            for q=1:length(tmp)
                                tmp(q) = obj.distance(Xtr(p,:),Xtr(notClusterMembersID(q),:));
                            end
                            b_tmp = [b_tmp mean(tmp)];
                    end
                    %                     b_tmp = [b_tmp mean(pdist2(Xtr(p,:),notClusterMembers,'mahalanobis',COVARIANCES{k}))];
                end
            end
            b(p)=min(b_tmp);

            s(p)=(b(p)-a(p))/max(a(p),b(p));
        end
        SILHOUETTE(k) = mean(s);
    end
    fprintf('done! [Time elapsed: %.2f sec]\n\n',toc);
else
    SILHOUETTE = NaN(K,1); % NaN as a dummy value if user does not want Silhouette
end

%%% Output
outputArg = {FEASIBILITY,CENTROIDS,MEMBERSHIPS,HYPERPLANES,COVARIANCES,CARDINALITIES,DBI,SILHOUETTE,MAXANFISMF};
end

%% More functions
function [Ahat, froNorm] = fixCovarianceMatrix(A)
 
% symmetrize A into B
B = (A + A')/2;
 
% Compute the symmetric polar factor of B. Call it H.
% Clearly H is itself SPD.
[~,Sigma,V] = svd(B);
H = V*Sigma*V';
 
% get Ahat in the above formula
Ahat = (B+H)/2;
 
% ensure symmetry
Ahat = (Ahat + Ahat')/2;
 
% test that Ahat is in fact PD. if it is not so, then tweak it just a bit.
p = 1;
k = 0;
while p ~= 0
    [~,p] = chol(Ahat);
    k = k + 1;
    if p ~= 0
        % Ahat failed the chol test. It must have been just a hair off,
        % due to floating point trash, so it is simplest now just to
        % tweak by adding a tiny multiple of an identity matrix.
        mineig = min(eig(Ahat));
        Ahat = Ahat + (-mineig*k.^2 + eps(mineig))*eye(size(A));
    end
    if k>=100
        Ahat = Ahat + k^2*eps*eye(size(Ahat));
    end
end
froNorm = norm(Ahat-A,'fro');

end