function obj = fit(obj,Xtr,Ytr)
%METHOD1 Summary of this method goes here
%   Detailed explanation goes here
[FEASIBILITY,CENTROIDS,MEMBERSHIPS,HYPERPLANES,COVARIANCES,CARDINALITIES,DBI,SILHOUETTE,MAXANFISMF] = deal(cell(obj.numClusters(end),1));
switch obj.mode
    case 'partitional'
        for K = obj.numClusters
            outputArg = obj.fitPartitional(Xtr,Ytr,K);
            FEASIBILITY{K} = outputArg{1};
            CENTROIDS{K} = outputArg{2};
            MEMBERSHIPS{K} = outputArg{3};
            HYPERPLANES{K} = outputArg{4};
            COVARIANCES{K} = outputArg{5};
            CARDINALITIES{K} = outputArg{6};
            DBI{K} = outputArg{7};
            SILHOUETTE{K} = outputArg{8};
            MAXANFISMF{K} = outputArg{9};
        end
        % results as a tree
        for K = obj.numClusters
            FEASIBILITY(K,1:K) = FEASIBILITY{K};
            CENTROIDS(K,1:K) = CENTROIDS{K};
            MEMBERSHIPS(K,1:K) = MEMBERSHIPS{K};
            HYPERPLANES(K,1:K) = HYPERPLANES{K};
            COVARIANCES(K,1:K) = COVARIANCES{K};
            CARDINALITIES(K,1:K) = CARDINALITIES{K};
            % DBI(K,1:K) = DBI{K};
            % SILHOUETTE(K,1:K) = SILHOUETTE{K};
            MAXANFISMF(K,1:K) = MAXANFISMF{K};
        end
        DBI = DBI(:,1);
        SILHOUETTE = SILHOUETTE(:,1);
    case 'agglomerative'
        outputArg = obj.fitAgglomerative(Xtr,Ytr,max(obj.numClusters));
        FEASIBILITY = outputArg{1};
        CENTROIDS = outputArg{2};
        MEMBERSHIPS = outputArg{3};
        HYPERPLANES = outputArg{4};
        COVARIANCES = outputArg{5};
        CARDINALITIES = outputArg{6};
        DBI = outputArg{7};
        SILHOUETTE = outputArg{8};
        MAXANFISMF = outputArg{9};
    case 'divisive'
        outputArg = obj.fitDivisive(Xtr,Ytr,max(obj.numClusters));
        FEASIBILITY = outputArg{1};
        CENTROIDS = outputArg{2};
        MEMBERSHIPS = outputArg{3};
        HYPERPLANES = outputArg{4};
        COVARIANCES = outputArg{5};
        CARDINALITIES = outputArg{6};
        DBI = outputArg{7};
        SILHOUETTE = outputArg{8};
        MAXANFISMF = outputArg{9};
end
%% ALESSIO HACK: Option 1 (remove clusters)
%     % ALESSIO HACK in v5 (remove clusters with low cardinality)
%     % remove low cardinality
%     for i = 1:size(FEASIBILITY,1)
%         for j = 1:size(FEASIBILITY,2)
%             if isempty(FEASIBILITY{i,j})==false && FEASIBILITY{i,j}==0
%                 FEASIBILITY{i,j} = [];
%                 CENTROIDS{i,j} = [];
%                 MEMBERSHIPS{i,j} = [];
%                 HYPERPLANES{i,j} = [];
%                 COVARIANCES{i,j} = [];
%                 CARDINALITIES{i,j} = [];
%                 MAXANFISMF{i,j} = [];
%             end
%         end
%     end
%     % make surviving clusters continuous in cell arrays
%     tmp = double(cellfun(@(x) isempty(x),FEASIBILITY));
%     [~, idx] = sort(tmp,2);
%     for i = 1:size(FEASIBILITY,1)
%         FEASIBILITY(i,:) = FEASIBILITY(i,idx(i,:));
%         CENTROIDS(i,:) = CENTROIDS(i,idx(i,:));
%         MEMBERSHIPS(i,:) = MEMBERSHIPS(i,idx(i,:));
%         HYPERPLANES(i,:) = HYPERPLANES(i,idx(i,:));
%         COVARIANCES(i,:) = COVARIANCES(i,idx(i,:));
%         CARDINALITIES(i,:) = CARDINALITIES(i,idx(i,:));
%         MAXANFISMF(i,:) = MAXANFISMF(i,idx(i,:));
%     end
%% ALESSIO HACK: Option 2 (constant hyperplane)
for i = 1:size(FEASIBILITY,1)
    for j = 1:size(FEASIBILITY,2)
        if isempty(FEASIBILITY{i,j})==false && FEASIBILITY{i,j}==0
            if CARDINALITIES{i,j}>1
                FEASIBILITY{i,j} = true;   % now we mark this solution as feasible
                HYPERPLANES{i,j} = [mean(Ytr(MEMBERSHIPS{i,j},:),1); zeros(size(Xtr,2),1)];	% add here                
                %COVARIANCES{i,j} = eye(size(Xtr,2))*max(std( Xtr(MEMBERSHIPS{i,j},:))); % ones(size(Xtr,2));  % add here
                COVARIANCES{i,j} = eye(size(Xtr,2)) * mean(pdist2( (Xtr(MEMBERSHIPS{i,j},:)), CENTROIDS{i,j}));
            elseif CARDINALITIES{i,j}==1
                FEASIBILITY{i,j} = true;   % now we mark this solution as feasible
                HYPERPLANES{i,j} = [mean(Ytr(MEMBERSHIPS{i,j},:),1); zeros(size(Xtr,2),1)];	% add here
                COVARIANCES{i,j} = eye(size(Xtr,2)) * eps;
            end
        end
    end
end
%%
%%% Output
obj.feasibility = FEASIBILITY;
obj.centroids = CENTROIDS;
obj.memberships = MEMBERSHIPS;
obj.hyperplanes = HYPERPLANES;
obj.covariances = COVARIANCES;
obj.cardinalities = CARDINALITIES;
obj.DBI = DBI;
obj.silhouette = SILHOUETTE;
obj.maxAnfisMF = MAXANFISMF;
end