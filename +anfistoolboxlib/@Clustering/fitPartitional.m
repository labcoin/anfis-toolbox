function outputArg = fitPartitional(obj,Xtr,Ytr,K)
 
%%% input consistency check
[numPatterns, numFeatures] = size(Xtr);
Ytr = Ytr(:);   % make sure Ytr is a column vector
 
if length(Ytr)~=numPatterns
    error('Number of patterns and number of labels mismatch.');
end
if mod(K,1)~=0
    error('Parameter ''K'' must be integer.');
end
if K<=0 || K>size(unique(Xtr,'rows'),1)
    error('Parameter ''K'' must be greater than zero and at most equal to the number of distinct patterns in DATASET.');
end
if mod(obj.numReplicates,1)~=0
    error('Parameter ''numReplicates'' must be integer.');
end
if mod(obj.maxIters,1)~=0
    error('Parameter ''maxIters'' must be integer.');
end
if obj.epsilon<0 || obj.epsilon>1
    error('Parameter ''epsilon'' must be in range [0,1].');
end
if obj.parallel == false
    parforArg = 0;
else
    parforArg = Inf;
end
 
%%% init data structures for replicates storage
MEMBERSHIPS=cell(1,obj.numReplicates);
INERTIA=cell(1,obj.numReplicates);
CENTROIDS=cell(1,obj.numReplicates);
COVARIANCES=cell(obj.numReplicates,K);
HYPERPLANES=cell(obj.numReplicates,K);
CARDINALITIES=cell(obj.numReplicates,K);
 
%%% start replicates
parfor(rep = 1:obj.numReplicates,parforArg)
%for rep = 1:obj.numReplicates
    tic;
    % select initial centroids and hyperplanes [old]
    %     hyperplanes=cell(1,K);
    %     centres = zeros(K,numFeatures);
    %     numHyperplanes = K;
    %     patternPerHyperplane = floor(numPatterns/K);
    %     idx = randperm(numHyperplanes*patternPerHyperplane);
    %     idx = reshape(idx, numHyperplanes, patternPerHyperplane);
    %     for k = 1:K
    %         patternsInCluster = Xtr(idx(k,:),:);
    %         labelsInCluster = Ytr(idx(k,:));
    %         hyperplanes{k} = pinv([ones(size(patternsInCluster,1),1) patternsInCluster])*labelsInCluster;
    %         centres(k,:) = Xtr(idx(k,1),:);
    %     end
    %     memberships = zeros(size(Xtr,1),1);
    %     for k = 1:K
    %         memberships(idx(k,:),:)=k;
    %     end
    % select initial centroids and hyperplanes [new]
    hyperplanes=cell(1,K);
    if strcmp(obj.inputDistance,'mahalanobis')
        [centres, tmp_cov, badReplicate] = initFunctionMahalanobis(Xtr, K);
        if badReplicate == true
            disp('Replicate failed due to bad initialisation.'); % beep;
            INERTIA{rep} = -Inf;
            continue;
        end
        centres = cell2mat(centres');
    else
        centres = randsample(1:numPatterns,K,false);
        centres = Xtr(centres,:);
    end
    switch obj.inputDistance
        case 'euclidean'
            distanceMatrix = pdist2(Xtr,centres,'euclidean');
        case 'mahalanobis'
            distanceMatrix = zeros(size(Xtr,1), K);
            for k = 1:K
                try
                    distanceMatrix(:,k) = pdist2(Xtr,centres(k,:),'mahalanobis',tmp_cov{k});
                catch
                    try
                        distanceMatrix(:,k) = pdist2(Xtr,centres(k,:),'mahalanobis',fixCovarianceMatrix(tmp_cov{k}));
                    catch % if singleton
                        distanceMatrix(:,k) = pdist2(Xtr,centres(k,:),'euclidean');
                    end
                end
            end
        case 'sqEuclidean'
            distanceMatrix = pdist2(Xtr,centres,'euclidean').^2;
        case 'manhattan'
            distanceMatrix = pdist2(Xtr,centres,'cityblock');
        otherwise
            distanceMatrix = zeros(size(Xtr,1),1);
            for p=1:size(Xtr,1)
                for q=1:size(centres,1)
                    distanceMatrix(p,q) = obj.distance(Xtr(p,:),centres(q,:));
                end
            end
    end
    [~, memberships] = min(distanceMatrix,[],2);
    for k = 1:K
        patternsInCluster = Xtr(memberships==k,:);
        labelsInCluster = Ytr(memberships==k);
        hyperplanes{k} = pinv([ones(size(patternsInCluster,1),1) patternsInCluster])*labelsInCluster;
    end
    % if there are some empty clusters, initialize new cluster(s) with farthest point(s)
    if length(unique(memberships))~=K
        % warning('Empty cluster(s)! Reassigning...');
        emptyClustersID=setdiff(1:K,unique(memberships));
        changed = [];
        while ~isempty(emptyClustersID)
            farthestPoint = find(distanceMatrix == max(distanceMatrix));             % select farthest point from its representative
            farthestPoint = setdiff(farthestPoint, changed);
            farthestPoint = farthestPoint(1);
            changed = [changed farthestPoint];
            memberships(farthestPoint) = emptyClustersID(1);        % change membership with empty cluster
            centres(emptyClustersID(1),:) = Xtr(farthestPoint,:);   % change representative with pattern itself
            distanceMatrix(farthestPoint) = 0;                   % mark the distance with itself to 0
            emptyClustersID = setdiff(1:K,unique(memberships));       % update empty cluster IDs after reassignment
        end
    end
    % start Voronoi iterations
    for iter = 1:obj.maxIters
        % backup previous centroids for stopping criterion
        previousCentres = centres;
        % expectation (assign pattern to closest cluster)
        switch obj.inputDistance
            case 'euclidean'
                inputSpaceDist = pdist2(Xtr,centres,'euclidean');
            case 'sqEuclidean'
                inputSpaceDist = pdist2(Xtr,centres,'euclidean').^2;
            case 'manhattan'
                inputSpaceDist = pdist2(Xtr,centres,'cityblock');
            case 'mahalanobis'
                inputSpaceDist = zeros(numPatterns,K);
                for k = 1:K
                    try
                        inputSpaceDist(:,k) = pdist2(Xtr,centres(k,:),'mahalanobis',nancov(Xtr(memberships==k,:)));
                    catch
                        try
                            inputSpaceDist(:,k) = pdist2(Xtr,centres(k,:),'mahalanobis',fixCovarianceMatrix(nancov(Xtr(memberships==k,:))));
                        catch % if singleton
                            inputSpaceDist(:,k) = pdist2(Xtr,centres(k,:),'euclidean');
                        end
                    end
                    %                     try
                    %                         inputSpaceDist(:,k) = pdist2(Xtr,centres(k,:),'mahalanobis',nancov(Xtr(memberships==k,:)));
                    %                     catch
                    %                         %                         inputSpaceDist(:,k) = pdist2(Xtr,centres(k,:),'euclidean');
                    %                         inputSpaceDist(:,k) = sqrt(diag((Xtr-centres(k,:))*pinv(nancov(Xtr(memberships==k,:)))*(Xtr-centres(k,:))'));
                    %                     end
                end
            otherwise
                inputSpaceDist = zeros(numPatterns,K);
                for p=1:numPatterns
                    for k=1:K
                        inputSpaceDist(p,k) = obj.distance(Xtr(p,:),centres(k,:));
                    end
                end
        end
        hyperplanesSpaceDist = (Ytr - [ones(numPatterns,1) Xtr] * cell2mat(hyperplanes)).^2;
        distanceMatrix = obj.epsilon*inputSpaceDist + (1-obj.epsilon)*hyperplanesSpaceDist;
        [pointToCenterDist, memberships] = min(distanceMatrix,[],2);
        % if there are some empty clusters, initialize new cluster(s) with farthest point(s)
        if length(unique(memberships))~=K
            % warning('Empty cluster(s)! Reassigning...');
            emptyClustersID=setdiff(1:K,unique(memberships));
            changed = [];
            while ~isempty(emptyClustersID)
                farthestPoint = find(pointToCenterDist == max(pointToCenterDist));             % select farthest point from its representative
                farthestPoint = setdiff(farthestPoint, changed);
                farthestPoint = farthestPoint(1);
                changed = [changed farthestPoint];
                memberships(farthestPoint) = emptyClustersID(1);        % change membership with empty cluster
                centres(emptyClustersID(1),:) = Xtr(farthestPoint,:);   % change representative with pattern itself
                pointToCenterDist(farthestPoint) = 0;                   % mark the distance with itself to 0
                emptyClustersID = setdiff(1:K,unique(memberships));     % update empty cluster IDs after reassignment
            end
        end
        % maximization (update cluster representatives)
        for k = 1:K
            patternsInCluster = Xtr(memberships==k,:);
            labelsInCluster = Ytr(memberships==k);
            % update representatives
            switch obj.representative
                case 'medoid'
                    switch obj.inputDistance
                        case 'euclidean'
                            distanceMatrix = pdist2(patternsInCluster,patternsInCluster,'euclidean');
                        case 'mahalanobis'
                            try
                                distanceMatrix = pdist2(patternsInCluster,patternsInCluster,'mahalanobis', nancov(patternsInCluster));
                            catch
                                try
                                    distanceMatrix = pdist2(patternsInCluster,patternsInCluster,'mahalanobis', fixCovarianceMatrix(nancov(patternsInCluster)));
                                catch % if singleton
                                    distanceMatrix = pdist2(patternsInCluster,patternsInCluster,'euclidean');
                                end
                            end
                        case 'sqEuclidean'
                            distanceMatrix = pdist2(patternsInCluster,patternsInCluster,'euclidean').^2;
                        case 'manhattan'
                            distanceMatrix = pdist2(patternsInCluster,patternsInCluster,'cityblock');
                        otherwise
                            distanceMatrix = zeros(length(labelsInCluster));
                            for p=1:length(labelsInCluster)
                                for q=1:length(labelsInCluster)
                                    distanceMatrix(p,q) = obj.distance(patternsInCluster(p,:),patternsInCluster(q,:));
                                end
                            end
                    end
                    [~,medoidID] = min(sum(distanceMatrix,1));
                    centres(k,:) = patternsInCluster(medoidID,:);
                case 'centroid'
                    centres(k,:) = mean(patternsInCluster,1);
                case 'median'
                    centres(k,:) = median(patternsInCluster,1);
            end
            % update hyperplane
            hyperplanes{k} = pinv([ones(size(patternsInCluster,1),1) patternsInCluster])*labelsInCluster;
        end
        % stopping criterion
        if isequal(previousCentres,centres) || iter==obj.maxIters
            COVARIANCES(rep,:)=arrayfun(@(k) cov(Xtr(memberships==k,:)),1:K,'UniformOutput',false);
            MEMBERSHIPS{rep}=memberships;
            CENTROIDS{rep}=centres;
            HYPERPLANES(rep,:)=hyperplanes;
            % check whether all clusters contain a number of patterns at
            % least equal to the number of features plus one: if so, we consider the
            % hyperplane (and the cluster) valid, otherwise we mark the
            % WCSoD as infinite [REMOVED by ALESSIO in v5]
            [cardinalities, ~] = hist(memberships,unique(memberships));
            CARDINALITIES(rep,:) = num2cell(cardinalities);
            %             if sum(cardinalities<=numFeatures)>0
            %                 switch obj.inputDistance
            %                     case {'euclidean', 'sqEuclidean', 'manhattan'}
            %                         INERTIA{rep} = Inf;
            %                     case 'mahalanobis'
            %                         INERTIA{rep} = -Inf;
            %                 end
            %             else
            switch obj.inputDistance
                case 'mahalanobis'
                    INERTIA{rep} = objFunctionMahalanobis(MEMBERSHIPS{rep}, COVARIANCES(rep,:));
                case {'euclidean', 'sqEuclidean', 'manhattan'}
                    INERTIA{rep} = sum(pointToCenterDist);
            end
            %             end
            % summary
            fprintf(['Replicate %d out of %d for k = %d:\n' ...
                '\tConvergence reached after %d iterations\n' ...
                '\tObjective function: %.2f\n' ...
                '\tTime elapsed: %.2f sec\n\n'] ...
                ,rep,obj.numReplicates,K,iter,INERTIA{rep},toc);
            break
        end
    end
end
 
% return lowest WCSoD solution amongst replicates
switch obj.inputDistance
    case 'mahalanobis'
        [~,bestSolution] = max(cell2mat(INERTIA));
        fprintf('best WCSOD, %f \n', max(cell2mat(INERTIA)))
    case {'euclidean', 'sqEuclidean', 'manhattan'}
        [~,bestSolution] = min(cell2mat(INERTIA));
        fprintf('best WCSOD, %f \n', min(cell2mat(INERTIA)))
end
 
% [REMOVED by ALESSIO in v5]
% if all(cell2mat(INERTIA)==Inf) || all(cell2mat(INERTIA)==-Inf)
%     FEASIBILITY = num2cell(zeros(1, K));
%     outputArg = {FEASIBILITY,{NaN},{NaN},{NaN},{NaN},{NaN},{NaN},{NaN},{NaN}};
%     return;
% end
CENTROIDS = CENTROIDS{bestSolution};
MEMBERSHIPS = MEMBERSHIPS{bestSolution};
HYPERPLANES = HYPERPLANES(bestSolution,:);
COVARIANCES = COVARIANCES(bestSolution,:);
CARDINALITIES = CARDINALITIES(bestSolution,:);
% for the sake of ease, convert CENTROIDS to cell array (as HYPERPLANE and COVARIANCES)
if isequal(obj.inputDistance,'mahalanobis') && max(cell2mat(INERTIA)) == -inf
    CENTROIDS = mat2cell(zeros(K, numFeatures),ones(1,K),numFeatures)';
else
    CENTROIDS = mat2cell(CENTROIDS,ones(1,K),numFeatures)';
end
 
%%% Other output parameters
CLUSTER_PATTERNS = arrayfun(@(k) Xtr(MEMBERSHIPS==k,:),1:K,'UniformOutput',false);
% Feasibility
if isequal(obj.inputDistance,'mahalanobis') && max(cell2mat(INERTIA)) == -inf
    FEASIBILITY = num2cell(logical(zeros(size(CARDINALITIES))*false));
else
    FEASIBILITY = cellfun(@(x) x>numFeatures,CARDINALITIES,'UniformOutput',false);
end
% Max of Membership Function
MAXANFISMF = cellfun(@(x) (2*pi)^(-numFeatures/2) * det(x)^(-1/2), COVARIANCES, 'UniformOutput',false);
% Reformat MEMBERSHIPS as cell of indices
tmp_memberships = MEMBERSHIPS;
MEMBERSHIPS = arrayfun(@(k) find(tmp_memberships==k),1:K,'UniformOutput',false);
 
clear tmp_memberships;
 
%%% Davies-Bouldin Index
if obj.DaviesBouldin == true
    if K==1
        DBI = NaN;  % DBI is not defined for k = 1 cluster
    else
        tic; fprintf('\nEvaluating Davies-Bouldin Index...');
        s = zeros(1,K);
        Rij = zeros(K);
        % evaluating pairwise distances between clusters (representatives)
        representatives = CENTROIDS(cellfun(@(x) ~isempty(x),CENTROIDS));
        representatives = reshape(cell2mat(representatives),numFeatures,[])';
        %         switch algorithm
        %             case 'kmeans'
        %                 d = pdist2(representatives_asmatrix,representatives_asmatrix,'euclidean').^2;
        %             case 'kmedians'
        %                 d = pdist2(representatives_asmatrix,representatives_asmatrix,'cityblock');
        %             case 'kmedoids'
        %                 d = pdist2(representatives_asmatrix,representatives_asmatrix,'euclidean');
        %         end
        switch obj.inputDistance
            case {'euclidean', 'mahalanobis'}
                d = pdist2(representatives,representatives,'euclidean');
            case 'sqEuclidean'
                d = pdist2(representatives,representatives,'euclidean').^2;
            case 'manhattan'
                d = pdist2(representatives,representatives,'cityblock');
            otherwise
                d = zeros(size(representatives,1));
                for p=1:size(representatives,1)
                    for q=1:size(representatives,1)
                        d(p,q) = obj.distance(representatives(p,:),representatives(q,:));
                    end
                end
        end
        % evaluating intra-cluster average distance
        for i=1:K
            %             patternsInCluster = CLUSTER_PATTERNS{k,i};
            %             switch algorithm
            %                 case 'kmeans'
            %                     s(i) = mean(pdist2(patternsInCluster,representatives{i},'euclidean').^2);
            %                 case 'kmedians'
            %                     s(i) = mean(pdist2(patternsInCluster,representatives{i},'cityblock'));
            %                 case 'kmedoids'
            %                     s(i) = mean(pdist2(patternsInCluster,representatives{i},'mahalanobis',COVARIANCES{k,i}));
            %             end
            switch obj.inputDistance
                case 'euclidean'
                    s(i) = mean(pdist2(CLUSTER_PATTERNS{i},representatives(i,:),'euclidean'));
                case 'mahalanobis'
                    s(i) = mean(pdist2(CLUSTER_PATTERNS{i},representatives(i,:),'mahalanobis',COVARIANCES{k,i}));
                case 'sqEuclidean'
                    s(i) = mean(pdist2(CLUSTER_PATTERNS{i},representatives(i,:),'euclidean').^2);
                case 'manhattan'
                    s(i) = mean(pdist2(CLUSTER_PATTERNS{i},representatives(i,:),'cityblock'));
                otherwise
                    tmp = zeros(size(CLUSTER_PATTERNS{i},1),1);
                    for p=1:length(tmp)
                        tmp(p) = obj.distance(representatives(i,:),CLUSTER_PATTERNS{i}(p,:));
                    end
                    s(i) = mean(tmp);
            end
        end
        % evaluating cross-variance between clusters
        for i=1:K
            for j=1:K
                Rij(i,j)=(s(i)+s(j))/(d(i,j));
            end
        end
        Rij(1:K+1:end) = -1; % place -1 on diagonal to trick the max operator
        R = max(Rij,[],2);
        DBI = sum(R)/K;
    end
    fprintf('done!\t\t\t[Time elapsed: %.2f]\n',toc);
elseif obj.DaviesBouldin == false
    DBI = NaN; % NaN as a dummy value if user does not want DBI
end
 
%%% Silhouette
if obj.Silhouette == true
    fprintf('Evaluating the Silhouette...'); tic;
    %     for k = 2:K
    [a,b,s]=deal(zeros(numPatterns,1));
    parfor(p=1:numPatterns,parforArg) % can be done in parallel
        clusterID = cellfun(@(x) find(x==p),MEMBERSHIPS,'UniformOutput',false);
        clusterID = find(cellfun(@(x) ~isempty(x),clusterID));
        clusterMembersID = MEMBERSHIPS{clusterID};
        
        %         a_tmp = zeros(length(clusterMembersID),1);
        %         for i = 1:length(clusterMembersID)
        %             a_tmp(i) = obj.joinedDistance(Xtr(p,:), Xtr(clusterMembersID(i),:), ) %pattern,label,centroid,hyperplane,cluster,epsilon
        %         end
        
        switch obj.inputDistance
            case 'euclidean'
                a_tmp = pdist2(Xtr(p,:),Xtr(clusterMembersID,:),'euclidean');
            case 'mahalanobis'
                a_tmp = pdist2(Xtr(p,:),Xtr(clusterMembersID,:),'mahalanobis',COVARIANCES{clusterID});
            case 'sqEuclidean'
                a_tmp = pdist2(Xtr(p,:),Xtr(clusterMembersID,:),'euclidean').^2;
            case 'manhattan'
                a_tmp = pdist2(Xtr(p,:),Xtr(clusterMembersID,:),'cityblock');
            otherwise
                a_tmp = zeros(length(clusterMembersID),1);
                for q=1:length(a_tmp)
                    a_tmp(q) = obj.distance(Xtr(p,:),Xtr(clusterMembersID(q),:));
                end
        end
        a(p) = sum(a_tmp) / (length(clusterMembersID)-1);
        
        b_tmp = [];
        for i = 1:K
            if i~=clusterID
                notClusterMembersID = MEMBERSHIPS{i};
                switch obj.inputDistance
                    case 'euclidean'
                        b_tmp = [b_tmp mean(pdist2(Xtr(p,:),Xtr(notClusterMembersID,:),'euclidean'))];
                    case 'mahalanobis'
                        b_tmp = [b_tmp mean(pdist2(Xtr(p,:),Xtr(notClusterMembersID,:),'mahalanobis',COVARIANCES{i}))];
                    case 'sqEuclidean'
                        b_tmp = [b_tmp mean(pdist2(Xtr(p,:),Xtr(notClusterMembersID,:),'euclidean').^2)];
                    case 'manhattan'
                        b_tmp = [b_tmp mean(pdist2(Xtr(p,:),Xtr(notClusterMembersID,:),'cityblock'))];
                    otherwise
                        tmp = zeros(length(notClusterMembersID),1);
                        for q=1:length(tmp)
                            tmp(q) = obj.distance(Xtr(p,:),Xtr(notClusterMembersID(q),:));
                        end
                        b_tmp = [b_tmp mean(tmp)];
                end
                %                     b_tmp = [b_tmp mean(pdist2(Xtr(p,:),notClusterMembers,'mahalanobis',COVARIANCES{k}))];
            end
        end
        b(p)=min(b_tmp);
        
        s(p)=(b(p)-a(p))/max(a(p),b(p));
    end
    SILHOUETTE = mean(s);
    %     end
    fprintf('done!\t\t\t[Time elapsed: %.2f sec]\n\n',toc);
else
    SILHOUETTE = NaN; % NaN as a dummy value if user does not want Silhouette
end
 
%%% Output
outputArg = {FEASIBILITY,CENTROIDS,MEMBERSHIPS,HYPERPLANES,COVARIANCES,CARDINALITIES,DBI,SILHOUETTE,MAXANFISMF};
end

%% More functions
function A = objFunctionMahalanobis(memberships, covariances)
 
% eigs = cellfun(@(x) eig(x), covariances, 'UniformOutput',false);
% logEigs = cellfun(@(x) log(x), eigs, 'UniformOutput',false);
% sumLogEigs = cellfun(@(x) sum(x), logEigs);
 
K = length(unique(memberships));
for k = 1:K
    [~,p] = chol(covariances{k});
    if p~=0
        covariances{k} = fixCovarianceMatrix(covariances{k});
    end
end
 
sumLogEigs = cellfun(@(x) sum(log(eig(x))), covariances);
sumLogEigs = real(sumLogEigs); % minor numerical errors
for k = 1:K
    n(k) = sum(memberships==k);
end
 
A = - sum(n.*sumLogEigs);
 
if A==Inf
    A=-Inf;
end
 
end
 
function [finalCentre, finalCovariance, badInit] = initFunctionMahalanobis(Xtr, K)
 
[finalID, finalCentre, finalCovariance] = deal(cell(1,K));
badInit = false;
 
for i=1:K
    if size(Xtr,1)==1       % exception for singleton cluster
        finalCentre{i} = Xtr;
        finalID{i} = 1;
        finalCovariance{i} = 1;
    else
        [finalID{i}, finalCentre{i}, finalCovariance{i}, isValid] = singleCluster(Xtr);
        if isValid == false
            badInit = true;
            [finalCentre, finalCovariance] = deal(NaN);
            return;
        end
    end
    % remove already selected patterns and ready for the next round
    Xtr(finalID{i},:) = [];
end
end
 
function [thisID, thisCentre, thisCovariance, isValid] = singleCluster(Xtr)
w = 20;
isValid = true;
[n, p] = size(Xtr);
 
dist = pdist2(Xtr,Xtr,'euclidean');
[dist, dist_id] = sort(dist, 2);
summedDist = sum(dist, 2);
[summedDist, summedDist_id] = sort(summedDist);
 
try
    centreID = randsample(summedDist_id,1,true,(n:-1:1).^2 / sum((1:n)/2));
catch
    isValid = false;
    [thisID, thisCentre, thisCovariance] = deal(NaN);
    return;
end
if size(dist_id,2)>=w
    neighboursID = dist_id(centreID, 1:w);
else
    neighboursID = dist_id(centreID, :);
end
neighboursID = setdiff(neighboursID, centreID); % remove itself
 
% init centre and covariance
centre{1} = mean(Xtr(neighboursID,:),1);
covariance{1} = nancov(Xtr(neighboursID,:));
 
% perform first iteration
try
    d = pdist2(Xtr,centre{1},'mahalanobis',covariance{1}).^2;
catch
    try
        d = pdist2(Xtr,centre{1},'mahalanobis', fixCovarianceMatrix(covariance{1})).^2;
    catch
        d = pdist2(Xtr,centre{1},'euclidean').^2;
    end
end
id = d < chi2inv(0.99,p);
centre{1} = mean(Xtr(id, :),1);
covariance{1} = cov(Xtr(id, :));
 
% perform iterations
for iter = 2:1000
    % backup for stopping criterion
    previousID = id;
    try
        d = pdist2(Xtr,centre{iter-1},'mahalanobis',covariance{iter-1}).^2;
    catch
        try
            d = pdist2(Xtr,centre{iter-1},'mahalanobis',fixCovarianceMatrix(covariance{iter-1})).^2;
        catch
            d = pdist2(Xtr,centre{1},'euclidean').^2;
        end
    end
    id = d < chi2inv(1-0.05,p);
    centre{iter} = mean(Xtr(id, :),1);
    covariance{iter} = nancov(Xtr(id, :));
    
    % stopping criterion
    if isequal(previousID, id)
        %disp('Convergence')
        break;
    end
end
 
thisID = id;
thisCentre = centre{iter};
thisCovariance = covariance{iter};
end
 
function [Ahat, froNorm] = fixCovarianceMatrix(A)
 
% symmetrize A into B
B = (A + A')/2;
 
% Compute the symmetric polar factor of B. Call it H.
% Clearly H is itself SPD.
[~,Sigma,V] = svd(B);
H = V*Sigma*V';
 
% get Ahat in the above formula
Ahat = (B+H)/2;
 
% ensure symmetry
Ahat = (Ahat + Ahat')/2;
 
% test that Ahat is in fact PD. if it is not so, then tweak it just a bit.
p = 1;
k = 0;
while p ~= 0
    [~,p] = chol(Ahat);
    k = k + 1;
    if p ~= 0
        % Ahat failed the chol test. It must have been just a hair off,
        % due to floating point trash, so it is simplest now just to
        % tweak by adding a tiny multiple of an identity matrix.
        mineig = min(eig(Ahat));
        Ahat = Ahat + (-mineig*k.^2 + eps(mineig))*eye(size(A));
    end
    if k>=100
        Ahat = Ahat + k^2*eps*eye(size(Ahat));
    end
end
froNorm = norm(Ahat-A,'fro');

end