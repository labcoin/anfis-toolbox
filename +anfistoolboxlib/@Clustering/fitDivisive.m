function outputArg = fitDivisive(obj,Xtr,Ytr,K)

%%% Regularisation term for badly-conditioned matrices
% global reg;
% reg = 10^(-7);

% strip flag for parallel computing
if obj.parallel == false
    parforArg = 0;
else
    parforArg = Inf;
end

[numPatterns,numFeatures]=size(Xtr);
epsilonError = -inf;
%%% initialisation (k=1)
tic; t=tic;
fprintf('Working on root node...');
CLUSTER_PATTERNS{1} = Xtr;
CLUSTER_LABELS{1} = Ytr;
MEMBERSHIPS{1} = (1:size(Xtr,1))';
HYPERPLANES{1} = pinv([ones(size(CLUSTER_PATTERNS{1},1),1) CLUSTER_PATTERNS{1}])*CLUSTER_LABELS{1};
switch obj.representative
    case 'medoid'
        switch obj.inputDistance
            case 'euclidean'
                distanceMatrix = pdist2(Xtr,Xtr,'euclidean');
            case 'mahalanobis'
                try
                    distanceMatrix = pdist2(Xtr,Xtr,'mahalanobis', nancov(Xtr));
                catch
                    try
                        distanceMatrix = pdist2(Xtr,Xtr,'mahalanobis',fixCovarianceMatrix(nancov(Xtr)));
                    catch
                        distanceMatrix = pdist2(Xtr,Xtr,'euclidean');
                    end
                end
            case 'sqEuclidean'
                distanceMatrix = pdist2(Xtr,Xtr,'euclidean').^2;
            case 'manhattan'
                distanceMatrix = pdist2(Xtr,Xtr,'cityblock');
            otherwise
                distanceMatrix = zeros(size(Xtr,1));
                for p=1:size(Xtr,1)
                    for q=1:size(Xtr,1)
                        distanceMatrix(p,q) = obj.distance(Xtr(p,:),Xtr(q,:));
                    end
                end
        end
        [~,medoidID] = min(sum(distanceMatrix,1));
        CENTROIDS{1} = Xtr(medoidID,:);
    case 'centroid'
        CENTROIDS{1} = mean(Xtr,1);
    case 'median'
        CENTROIDS{1} = median(Xtr,1);
end
fprintf('done!\t[Time elapsed: %.2f]\n',toc);
%%% first split (k=2)
tic; fprintf('Working on split #1...');
for rep = 1:obj.numReplicates
    % randomly init centroids and hyperplanes
    idx1 = randsample(1:numPatterns,round(numPatterns/2),false);
    idx2 = setdiff(1:numPatterns,idx1);
    HYPERPLANES{2,1} = pinv([ones(size(Xtr(idx1,:),1),1) Xtr(idx1,:)])*Ytr(idx1);
    HYPERPLANES{2,2} = pinv([ones(size(Xtr(idx2,:),1),1) Xtr(idx2,:)])*Ytr(idx2);
    CLUSTER_PATTERNS{2,1} = Xtr(idx1,:);
    CLUSTER_PATTERNS{2,2} = Xtr(idx2,:);
    CLUSTER_LABELS{2,1} = Ytr(idx1,:);
    CLUSTER_LABELS{2,2} = Ytr(idx2,:);
    centres = [Xtr(idx1(1),:) ; Xtr(idx2(1),:)];
    memberships(idx1,:)=1;
    memberships(idx2,:)=2;
    % start Voronoi
    for iter = 1:obj.maxIters
        previousCentres=centres;
        % expectation (assign pattern to closest cluster)
        switch obj.inputDistance
            case 'euclidean'
                inputSpaceDist = pdist2(Xtr,centres,'euclidean');
            case 'sqEuclidean'
                inputSpaceDist = pdist2(Xtr,centres,'euclidean').^2;
            case 'manhattan'
                inputSpaceDist = pdist2(Xtr,centres,'cityblock');
            case 'mahalanobis'
                inputSpaceDist = zeros(numPatterns,2);
                for k = 1:2
                    try
                        inputSpaceDist(:,k) = pdist2(Xtr,centres(k,:),'mahalanobis',nancov(Xtr(memberships==k,:)));
                    catch
                        try
                            inputSpaceDist(:,k) = pdist2(Xtr,centres(k,:),'mahalanobis',fixCovarianceMatrix(nancov(Xtr(memberships==k,:))));
                        catch
                            inputSpaceDist(:,k) = pdist2(Xtr,centres(k,:),'euclidean');
                        end
                    end
                end
            otherwise
                inputSpaceDist = zeros(numPatterns,2);
                for p=1:numPatterns
                    for k=1:2
                        inputSpaceDist(p,k) = obj.distance(Xtr(p,:),centres(k,:));
                    end
                end
        end
        hyperplanesSpaceDist = (Ytr - [ones(numPatterns,1) Xtr] * cell2mat(HYPERPLANES(2,:))).^2;
        distanceMatrix = obj.epsilon*inputSpaceDist + (1-obj.epsilon)*hyperplanesSpaceDist;
        [pointToCenterDist, memberships] = min(distanceMatrix,[],2);
        % check whether clusters are empty
        if length(unique(memberships))~=2
            %             warning('Empty cluster(s)! Reassigning...');
            emptyClustersID=setdiff(1:2,unique(memberships));
            for e = 1:length(emptyClustersID)
                [~,farthestPoint] = max(pointToCenterDist);
                memberships(farthestPoint) = emptyClustersID(e);
                centres(emptyClustersID(e),:) = Xtr(farthestPoint,:);
            end
        end
        % update centroids, memberships & hyperplanes
        for k = 1:2
            CLUSTER_PATTERNS{2,k} = Xtr(memberships==k,:);
            CLUSTER_LABELS{2,k} = Ytr(memberships==k);
            MEMBERSHIPS{2,k} = find(memberships==k);
            % update representatives
            switch obj.representative
                case 'medoid'
                    switch obj.inputDistance
                        case 'euclidean'
                            distanceMatrix = pdist2(CLUSTER_PATTERNS{2,k},CLUSTER_PATTERNS{2,k},'euclidean');
                        case 'mahalanobis'
                            try
                                distanceMatrix = pdist2(CLUSTER_PATTERNS{2,k},CLUSTER_PATTERNS{2,k},'mahalanobis', nancov(CLUSTER_PATTERNS{2,k}));
                            catch
                                try
                                    distanceMatrix = pdist2(CLUSTER_PATTERNS{2,k},CLUSTER_PATTERNS{2,k},'mahalanobis', fixCovarianceMatrix(nancov(CLUSTER_PATTERNS{2,k})));
                                catch
                                    distanceMatrix = pdist2(CLUSTER_PATTERNS{2,k},CLUSTER_PATTERNS{2,k},'euclidean');
                                end
                            end
                        case 'sqEuclidean'
                            distanceMatrix = pdist2(CLUSTER_PATTERNS{2,k},CLUSTER_PATTERNS{2,k},'euclidean').^2;
                        case 'manhattan'
                            distanceMatrix = pdist2(CLUSTER_PATTERNS{2,k},CLUSTER_PATTERNS{2,k},'cityblock');
                        otherwise
                            distanceMatrix = zeros(size(CLUSTER_PATTERNS{2,k},1));
                            for p=1:size(CLUSTER_PATTERNS{2,k},1)
                                for q=1:size(CLUSTER_PATTERNS{2,k},1)
                                    distanceMatrix(p,q) = obj.distance(CLUSTER_PATTERNS{2,k}(p,:),CLUSTER_PATTERNS{2,k}(q,:));
                                end
                            end
                    end
                    [~,medoidID] = min(sum(distanceMatrix,1));
                    centres(k,:) = CLUSTER_PATTERNS{2,k}(medoidID,:);
                case 'centroid'
                    centres(k,:) = mean(CLUSTER_PATTERNS{2,k},1);
                case 'median'
                    centres(k,:) = median(CLUSTER_PATTERNS{2,k},1);
            end
            % update hyperplane
            HYPERPLANES{2,k} = pinv([ones(size(CLUSTER_PATTERNS{2,k},1),1) CLUSTER_PATTERNS{2,k}])*CLUSTER_LABELS{2,k};
        end
        % stopping criteria
        if isequal(centres,previousCentres) || iter==obj.maxIters
            break;
        end
    end
    ERROR{2,1}=sum((CLUSTER_LABELS{2,1}-([ones(size(CLUSTER_PATTERNS{2,1},1),1) CLUSTER_PATTERNS{2,1}]*HYPERPLANES{2,1})).^2);
    ERROR{2,2}=sum((CLUSTER_LABELS{2,2}-([ones(size(CLUSTER_PATTERNS{2,2},1),1) CLUSTER_PATTERNS{2,2}]*HYPERPLANES{2,2})).^2);
    % store replicates in temporary structures
    tmp_cluster_patterns(rep,1:2) = CLUSTER_PATTERNS(2,1:2);
    tmp_cluster_labels(rep,1:2) = CLUSTER_LABELS(2,1:2);
    tmp_hyperplanes(rep,1:2) = HYPERPLANES(2,1:2);
    tmp_error(rep,1:2) = ERROR(2,1:2);
    tmp_memberships(rep,1:2)=MEMBERSHIPS(2,1:2);
    tmp_centres{rep,1} = centres(1,:);
    tmp_centres{rep,2} = centres(2,:);
end
% select splits with minimum mean error
[~,bestSplitID]=min(mean(cell2mat(tmp_error),2));
% housekeeping
CLUSTER_PATTERNS(2,1:2) = tmp_cluster_patterns(bestSplitID,1:2);
CLUSTER_LABELS(2,1:2) = tmp_cluster_labels(bestSplitID,1:2);
HYPERPLANES(2,1:2) = tmp_hyperplanes(bestSplitID,1:2);
ERROR(2,1:2) = tmp_error(bestSplitID,1:2);
MEMBERSHIPS(2,1:2) = tmp_memberships(bestSplitID,1:2);
CENTROIDS(2,1:2) = tmp_centres(bestSplitID,1:2);
clear tmp*;
fprintf('done!\t[Time elapsed: %.2f]\n',toc);

%%% more splits (k=2)
for lvl = 3:K
    tic; fprintf('Working on split #%d...',lvl-1);
    if lvl-1>=15
        %
    end
    % select cluster to split
    [maxError, badCluster]=max(cell2mat(ERROR(lvl-1,:)));
    goodClusters = setdiff(1:size(CLUSTER_PATTERNS,2),badCluster);
    if maxError<epsilonError
        fprintf('done!\t[Time elapsed: %.2f]\n',toc);
        break
    end
    
    % split cluster
    for rep = 1:obj.numReplicates
        % randomly init centroids and hyperplanes
        idx1 = randsample(1:size(CLUSTER_PATTERNS{lvl-1,badCluster},1),round(size(CLUSTER_PATTERNS{lvl-1,badCluster},1)/2),false);
        idx2 = setdiff(1:size(CLUSTER_PATTERNS{lvl-1,badCluster},1),idx1);
        HYPERPLANES{lvl,1} = pinv([ones(size(CLUSTER_PATTERNS{lvl-1,badCluster}(idx1,:),1),1) CLUSTER_PATTERNS{lvl-1,badCluster}(idx1,:)])*CLUSTER_LABELS{lvl-1,badCluster}(idx1);
        HYPERPLANES{lvl,2} = pinv([ones(size(CLUSTER_PATTERNS{lvl-1,badCluster}(idx2,:),1),1) CLUSTER_PATTERNS{lvl-1,badCluster}(idx2,:)])*CLUSTER_LABELS{lvl-1,badCluster}(idx2);
        
        CLUSTER_PATTERNS{lvl,1} = CLUSTER_PATTERNS{lvl-1,badCluster}(idx1,:);
        CLUSTER_PATTERNS{lvl,2} = CLUSTER_PATTERNS{lvl-1,badCluster}(idx2,:);
        CLUSTER_LABELS{lvl,1} = CLUSTER_LABELS{lvl-1,badCluster}(idx1,:);
        CLUSTER_LABELS{lvl,2} = CLUSTER_LABELS{lvl-1,badCluster}(idx2,:);
        centres = [CLUSTER_PATTERNS{lvl-1,badCluster}(idx1(1),:) ; CLUSTER_PATTERNS{lvl-1,badCluster}(idx2(1),:)];
        memberships(idx1,:)=1;
        memberships(idx2,:)=2;
        for iter=1:obj.maxIters
            previousCentres=centres;
            switch obj.inputDistance
                case 'euclidean'
                    inputSpaceDist = pdist2(CLUSTER_PATTERNS{lvl-1,badCluster},centres,'euclidean');
                case 'sqEuclidean'
                    inputSpaceDist = pdist2(CLUSTER_PATTERNS{lvl-1,badCluster},centres,'euclidean').^2;
                case 'manhattan'
                    inputSpaceDist = pdist2(CLUSTER_PATTERNS{lvl-1,badCluster},centres,'cityblock');
                case 'mahalanobis'
                    inputSpaceDist = zeros(size(CLUSTER_PATTERNS{lvl-1,badCluster},1),2);
                    for k = 1:2
                        try
                            inputSpaceDist(:,k) = pdist2(CLUSTER_PATTERNS{lvl-1,badCluster},centres(k,:),'mahalanobis',nancov(CLUSTER_PATTERNS{lvl-1,badCluster}(memberships==k,:)));
                        catch
                            try
                                inputSpaceDist(:,k) = pdist2(CLUSTER_PATTERNS{lvl-1,badCluster},centres(k,:),'mahalanobis',fixCovarianceMatrix(nancov(CLUSTER_PATTERNS{lvl-1,badCluster}(memberships==k,:))));
                            catch
                                inputSpaceDist(:,k) = pdist2(CLUSTER_PATTERNS{lvl-1,badCluster},centres(k,:),'euclidean');
                            end
                        end
                    end
                otherwise
                    inputSpaceDist = zeros(size(CLUSTER_PATTERNS{lvl-1,badCluster},1),2);
                    for p=1:numPatterns
                        for k=1:2
                            inputSpaceDist(p,k) = obj.distance(CLUSTER_PATTERNS{lvl-1,badCluster}(p,:),centres(k,:));
                        end
                    end
            end
            hyperplanesSpaceDist = (CLUSTER_LABELS{lvl-1,badCluster} - [ones(size(CLUSTER_PATTERNS{lvl-1,badCluster},1),1) CLUSTER_PATTERNS{lvl-1,badCluster}] * cell2mat(HYPERPLANES(lvl,1:2))).^2;
            distanceMatrix = obj.epsilon*inputSpaceDist + (1-obj.epsilon)*hyperplanesSpaceDist;
            [pointToCenterDist, memberships] = min(distanceMatrix,[],2);
            % check whether clusters are empty
            if length(unique(memberships))~=2
                %             warning('Empty cluster(s)! Reassigning...');
                emptyClustersID=setdiff(1:2,unique(memberships));
                for e = 1:length(emptyClustersID)
                    [~,farthestPoint] = max(pointToCenterDist);
                    memberships(farthestPoint) = emptyClustersID(e);
                    centres(emptyClustersID(e),:) = Xtr(farthestPoint,:);
                end
            end
            % update medoids, memberships & hyperplanes
            for k = 1:2
                CLUSTER_PATTERNS{lvl,k} = CLUSTER_PATTERNS{lvl-1,badCluster}(memberships==k,:);
                CLUSTER_LABELS{lvl,k} = CLUSTER_LABELS{lvl-1,badCluster}(memberships==k,:);
                MEMBERSHIPS{lvl,k} = MEMBERSHIPS{lvl-1,badCluster}(memberships==k);
                % (ALESSIO HACK in v5)
                %                 feasibleClusters_earlyStop = cellfun(@(x) size(x,1) > numFeatures,CLUSTER_PATTERNS(lvl,1:2));
                %                 if any(feasibleClusters_earlyStop==false)
                %                     break;
                %                 end
                % update representatives
                switch obj.representative
                    case 'medoid'
                        switch obj.inputDistance
                            case 'euclidean'
                                distanceMatrix = pdist2(CLUSTER_PATTERNS{lvl,k},CLUSTER_PATTERNS{lvl,k},'euclidean');
                            case 'mahalanobis'
                                try
                                    distanceMatrix = pdist2(CLUSTER_PATTERNS{lvl,k},CLUSTER_PATTERNS{lvl,k},'mahalanobis', nancov(CLUSTER_PATTERNS{lvl,k}));
                                catch
                                    try
                                        distanceMatrix = pdist2(CLUSTER_PATTERNS{lvl,k},CLUSTER_PATTERNS{lvl,k},'mahalanobis', fixCovarianceMatrix(nancov(CLUSTER_PATTERNS{lvl,k})));
                                    catch
                                        distanceMatrix = pdist2(CLUSTER_PATTERNS{lvl,k},CLUSTER_PATTERNS{lvl,k},'euclidean');
                                    end
                                end
                            case 'sqEuclidean'
                                distanceMatrix = pdist2(CLUSTER_PATTERNS{lvl,k},CLUSTER_PATTERNS{lvl,k},'euclidean').^2;
                            case 'manhattan'
                                distanceMatrix = pdist2(CLUSTER_PATTERNS{lvl,k},CLUSTER_PATTERNS{lvl,k},'cityblock');
                            otherwise
                                distanceMatrix = zeros(size(CLUSTER_PATTERNS{lvl,k},1));
                                for p=1:size(CLUSTER_PATTERNS{lvl,k},1)
                                    for q=1:size(CLUSTER_PATTERNS{lvl,k},1)
                                        distanceMatrix(p,q) = obj.distance(CLUSTER_PATTERNS{lvl,k}(p,:),CLUSTER_PATTERNS{lvl,k}(q,:));
                                    end
                                end
                        end
                        [~,medoidID] = min(sum(distanceMatrix,1));
                        centres(k,:) = CLUSTER_PATTERNS{lvl,k}(medoidID,:);
                    case 'centroid'
                        centres(k,:) = mean(CLUSTER_PATTERNS{lvl,k},1);
                    case 'median'
                        centres(k,:) = median(CLUSTER_PATTERNS{lvl,k},1);
                end
                % update hyperplane ALESSIO HACK
                if length(CLUSTER_LABELS{lvl,k}) > numFeatures
                    HYPERPLANES{lvl,k} = pinv([ones(size(CLUSTER_PATTERNS{lvl,k},1),1) CLUSTER_PATTERNS{lvl,k}])*CLUSTER_LABELS{lvl,k};
                else
                    HYPERPLANES{lvl,k} = [mean(CLUSTER_LABELS{lvl,k});  zeros(numFeatures,1) ];
                end
            end
            % stopping criteria
            if isequal(centres,previousCentres) || iter==obj.maxIters
                break;
            end
            % reprise (ALESSIO HACK in v5)
            %             if any(feasibleClusters_earlyStop==false)
            %                 break;
            %             end
        end
        % reprise (ALESSIO HACK in v5)
        %         if any(feasibleClusters_earlyStop==false)
        %             ERROR{lvl,1}=Inf;
        %             ERROR{lvl,2}=Inf;
        %         else
        %             ERROR{lvl,1}=sum((CLUSTER_LABELS{lvl,1}-([ones(size(CLUSTER_PATTERNS{lvl,1},1),1) CLUSTER_PATTERNS{lvl,1}]*HYPERPLANES{lvl,1})).^2);
        %             ERROR{lvl,2}=sum((CLUSTER_LABELS{lvl,2}-([ones(size(CLUSTER_PATTERNS{lvl,2},1),1) CLUSTER_PATTERNS{lvl,2}]*HYPERPLANES{lvl,2})).^2);
        %         end
        ERROR{lvl,1}=sum((CLUSTER_LABELS{lvl,1}-([ones(size(CLUSTER_PATTERNS{lvl,1},1),1) CLUSTER_PATTERNS{lvl,1}]*HYPERPLANES{lvl,1})).^2);
        ERROR{lvl,2}=sum((CLUSTER_LABELS{lvl,2}-([ones(size(CLUSTER_PATTERNS{lvl,2},1),1) CLUSTER_PATTERNS{lvl,2}]*HYPERPLANES{lvl,2})).^2);
        % store replicates in temporary structures
        tmp_cluster_patterns(rep,1:2) = CLUSTER_PATTERNS(lvl,1:2);
        tmp_cluster_labels(rep,1:2) = CLUSTER_LABELS(lvl,1:2);
        tmp_hyperplanes(rep,1:2) = HYPERPLANES(lvl,1:2);
        tmp_error(rep,1:2) = ERROR(lvl,1:2);
        tmp_memberships(rep,1:2)=MEMBERSHIPS(lvl,1:2);
        tmp_centres{rep,1} = centres(1,:);
        tmp_centres{rep,2} = centres(2,:);
    end
    % select splits with minimum mean error
    [~,bestSplitID]=min(mean(cell2mat(tmp_error),2));
    % housekeeping
    CLUSTER_PATTERNS(lvl,1:2)=tmp_cluster_patterns(bestSplitID,1:2);
    CLUSTER_LABELS(lvl,1:2)=tmp_cluster_labels(bestSplitID,1:2);
    HYPERPLANES(lvl,1:2)=tmp_hyperplanes(bestSplitID,1:2);
    ERROR(lvl,1:2)=tmp_error(bestSplitID,1:2);
    MEMBERSHIPS(lvl,1:2)=tmp_memberships(bestSplitID,1:2);
    CENTROIDS(lvl,1:2) = tmp_centres(bestSplitID,1:2);
    clear tmp*;
    
    % move good clusters to new level
    CLUSTER_PATTERNS(lvl,3:3+length(goodClusters)-1) = CLUSTER_PATTERNS(lvl-1,goodClusters);
    CLUSTER_LABELS(lvl,3:3+length(goodClusters)-1) = CLUSTER_LABELS(lvl-1,goodClusters);
    HYPERPLANES(lvl,3:3+length(goodClusters)-1) = HYPERPLANES(lvl-1,goodClusters);
    ERROR(lvl,3:3+length(goodClusters)-1) = ERROR(lvl-1,goodClusters);
    MEMBERSHIPS(lvl,3:3+length(goodClusters)-1) = MEMBERSHIPS(lvl-1,goodClusters);
    CENTROIDS(lvl,3:3+length(goodClusters)-1) = CENTROIDS(lvl-1,goodClusters);
    fprintf('done!\t[Time elapsed: %.2f]\n',toc);
end

%%% housekeeping
clear badCluster bestSplitID emptyClustersID goodClusters;
clear centroids distanceMatrix pointToCenterDist previousCentroids;
clear e farthestPoint p rep badCluster goodClusters iter k memberships;

%%% Finishing up
tic; fprintf('Finishing up...\n');

% evaluate clusters' cardinalities and feasibility
tic; fprintf('\tEvaluating feasibility...');
treeDepth = size(CLUSTER_PATTERNS,1);
fullCells = cellfun(@(x) ~isempty(x),CLUSTER_PATTERNS);

CARDINALITIES(fullCells) = cellfun(@(x) size(x,1),CLUSTER_PATTERNS(fullCells),'UniformOutput',false);
CARDINALITIES = reshape(CARDINALITIES,treeDepth,treeDepth);
FEASIBILITY(fullCells) = cellfun(@(x) x>numFeatures,CARDINALITIES(fullCells),'UniformOutput',false);
FEASIBILITY = reshape(FEASIBILITY,treeDepth,treeDepth);

for i=2:size(CLUSTER_PATTERNS,1)
    [~,idx]=sort(cell2mat(CARDINALITIES(i,1:i)));
    idx = fliplr(idx);
    CARDINALITIES(i,1:i) = CARDINALITIES(i,idx);
    FEASIBILITY(i,1:i) = FEASIBILITY(i,idx);
    HYPERPLANES(i,1:i) = HYPERPLANES(i,idx);
    CLUSTER_PATTERNS(i,1:i) = CLUSTER_PATTERNS(i,idx);
    CLUSTER_LABELS(i,1:i) = CLUSTER_LABELS(i,idx);
    MEMBERSHIPS(i,1:i) = MEMBERSHIPS(i,idx);
end

% return feasible solutions only
% [r,c]=find(cellfun(@(x) isempty(x)==false && x==0,FEASIBILITY));
% for i=1:length(r)
%     CARDINALITIES{r(i),c(i)} = [];
%     FEASIBILITY{r(i),c(i)} = [];
%     HYPERPLANES{r(i),c(i)} = [];
%     CLUSTER_PATTERNS{r(i),c(i)} = [];
%     CLUSTER_LABELS{r(i),c(i)} = [];
%     MEMBERSHIPS{r(i),c(i)} = [];
%     CENTROIDS{r(i),c(i)} = [];
% end
% r = min(r)-1;
% for i=r:size(FEASIBILITY,1)
%     FEASIBILITY(i,:) = {0};
% end
fullCells = cellfun(@(x) ~isempty(x),CLUSTER_PATTERNS);
fprintf('done!\t\t\t[Time elapsed: %.2f]\n',toc);

tic; fprintf('\tEvaluating clusters'' characteristics...');
[COVARIANCES, INVCOVARIANCES, MAXANFISMF] = deal(cell(size(CLUSTER_PATTERNS)));
for i = 1:size(CLUSTER_PATTERNS,1)
    for j = 1:size(CLUSTER_PATTERNS,2)
        if fullCells(i,j) == true
            COVARIANCES{i,j} = cov(CLUSTER_PATTERNS{i,j});
            [~,flagPD] = chol(COVARIANCES{i,j});
            if flagPD==0
                INVCOVARIANCES{i,j} = inv(COVARIANCES{i,j});
            else
                INVCOVARIANCES{i,j} = inv(fixCovarianceMatrix(COVARIANCES{i,j}));
            end
            MAXANFISMF{i,j} = (2*pi)^(-numFeatures/2) * det(COVARIANCES{i,j})^(-1/2);
        end
    end
end
fprintf('done!\t[Time elapsed: %.2f]\n',toc);

%%% Davies-Bouldin Index
if obj.DaviesBouldin == true
    DBI = zeros(K,1);
    tic; fprintf('\nEvaluating Davies-Bouldin Index...');
    for k = 2:K
        s = zeros(1,k);
        Rij = zeros(k);
        % evaluating pairwise distances between clusters (representatives)
        representatives = CENTROIDS(k,cellfun(@(x) ~isempty(x),CENTROIDS(k,:)));
        representatives = reshape(cell2mat(representatives),numFeatures,[])';
        %         switch algorithm
        %             case 'kmeans'
        %                 d = pdist2(representatives_asmatrix,representatives_asmatrix,'euclidean').^2;
        %             case 'kmedians'
        %                 d = pdist2(representatives_asmatrix,representatives_asmatrix,'cityblock');
        %             case 'kmedoids'
        %                 d = pdist2(representatives_asmatrix,representatives_asmatrix,'euclidean');
        %         end
        switch obj.inputDistance
            case {'euclidean', 'mahalanobis'}
                d = pdist2(representatives,representatives,'euclidean');
            case 'sqEuclidean'
                d = pdist2(representatives,representatives,'euclidean').^2;
            case 'manhattan'
                d = pdist2(representatives,representatives,'cityblock');
            otherwise
                d = zeros(size(representatives,1));
                for p=1:size(representatives,1)
                    for q=1:size(representatives,1)
                        d(p,q) = obj.distance(representatives(p,:),representatives(q,:));
                    end
                end
        end
        % evaluating intra-cluster average distance
        for i=1:k
            %             patternsInCluster = CLUSTER_PATTERNS{k,i};
            %             switch algorithm
            %                 case 'kmeans'
            %                     s(i) = mean(pdist2(patternsInCluster,representatives{i},'euclidean').^2);
            %                 case 'kmedians'
            %                     s(i) = mean(pdist2(patternsInCluster,representatives{i},'cityblock'));
            %                 case 'kmedoids'
            %                     s(i) = mean(pdist2(patternsInCluster,representatives{i},'mahalanobis',COVARIANCES{k,i}));
            %             end
            switch obj.inputDistance
                case 'euclidean'
                    s(i) = mean(pdist2(CLUSTER_PATTERNS{k,i},representatives(i,:),'euclidean'));
                case 'mahalanobis'
                    s(i) = mean(pdist2(CLUSTER_PATTERNS{k,i},representatives(i,:),'mahalanobis',COVARIANCES{k,i}));
                case 'sqEuclidean'
                    s(i) = mean(pdist2(CLUSTER_PATTERNS{k,i},representatives(i,:),'euclidean').^2);
                case 'manhattan'
                    s(i) = mean(pdist2(CLUSTER_PATTERNS{k,i},representatives(i,:),'cityblock'));
                otherwise
                    tmp = zeros(size(CLUSTER_PATTERNS{k,i},1),1);
                    for p=1:length(tmp)
                        tmp(p) = obj.distance(representatives(i,:),CLUSTER_PATTERNS{k,i}(p,:));
                    end
                    s(i) = mean(tmp);
            end
        end
        % evaluating cross-variance between clusters
        for i=1:k
            for j=1:k
                Rij(i,j)=(s(i)+s(j))/(d(i,j));
            end
        end
        Rij(1:k+1:end) = -1; % place -1 on diagonal to trick the max operator
        R = max(Rij,[],2);
        DBI(k) = sum(R)/k;
    end
    DBI(1) = NaN;   % DBI is not defined for k = 1 cluster
    fprintf('done!\t\t\t[Time elapsed: %.2f]\n',toc);
elseif obj.DaviesBouldin == false
    DBI = NaN(K,1); % NaN as a dummy value if user does not want DBI
end

%%% Silhouette
if obj.Silhouette == true
    fprintf('Evaluating the Silhouette...'); tic;
    SILHOUETTE = NaN(K,1);
    for k = 2:K
        [a,b,s]=deal(zeros(numPatterns,1));
        parfor(p=1:numPatterns,parforArg) % can be done in parallel
            clusterID = cellfun(@(x) find(x==p),MEMBERSHIPS(k,:),'UniformOutput',false);
            clusterID = find(cellfun(@(x) ~isempty(x),clusterID));
            clusterMembersID = MEMBERSHIPS{k,clusterID};
            
            %         a_tmp = zeros(length(clusterMembersID),1);
            %         for i = 1:length(clusterMembersID)
            %             a_tmp(i) = obj.joinedDistance(Xtr(p,:), Xtr(clusterMembersID(i),:), ) %pattern,label,centroid,hyperplane,cluster,epsilon
            %         end
            
            switch obj.inputDistance
                case 'euclidean'
                    a_tmp = pdist2(Xtr(p,:),Xtr(clusterMembersID,:),'euclidean');
                case 'mahalanobis'
                    a_tmp = pdist2(Xtr(p,:),Xtr(clusterMembersID,:),'mahalanobis',COVARIANCES{k,clusterID});
                case 'sqEuclidean'
                    a_tmp = pdist2(Xtr(p,:),Xtr(clusterMembersID,:),'euclidean').^2;
                case 'manhattan'
                    a_tmp = pdist2(Xtr(p,:),Xtr(clusterMembersID,:),'cityblock');
                otherwise
                    a_tmp = zeros(length(clusterMembersID),1);
                    for q=1:length(a_tmp)
                        a_tmp(q) = obj.distance(Xtr(p,:),Xtr(clusterMembersID(q),:));
                    end
            end
            a(p) = sum(a_tmp) / (length(clusterMembersID)-1);
            
            b_tmp = [];
            for i = 1:k
                if i~=clusterID
                    notClusterMembersID = MEMBERSHIPS{i};
                    switch obj.inputDistance
                        case 'euclidean'
                            b_tmp = [b_tmp mean(pdist2(Xtr(p,:),Xtr(notClusterMembersID,:),'euclidean'))];
                        case 'mahalanobis'
                            b_tmp = [b_tmp mean(pdist2(Xtr(p,:),Xtr(notClusterMembersID,:),'mahalanobis',COVARIANCES{k,i}))];
                        case 'sqEuclidean'
                            b_tmp = [b_tmp mean(pdist2(Xtr(p,:),Xtr(notClusterMembersID,:),'euclidean').^2)];
                        case 'manhattan'
                            b_tmp = [b_tmp mean(pdist2(Xtr(p,:),Xtr(notClusterMembersID,:),'cityblock'))];
                        otherwise
                            tmp = zeros(length(notClusterMembersID),1);
                            for q=1:length(tmp)
                                tmp(q) = obj.distance(Xtr(p,:),Xtr(notClusterMembersID(q),:));
                            end
                            b_tmp = [b_tmp mean(tmp)];
                    end
                end
            end
            b(p)=min(b_tmp);
            
            s(p)=(b(p)-a(p))/max(a(p),b(p));
        end
        SILHOUETTE(k) = mean(s);
    end
    fprintf('done! [Time elapsed: %.2f sec]\n\n',toc);
else
    SILHOUETTE = NaN(K,1); % NaN as a dummy value if user does not want Silhouette
end

fprintf('Total time elapsed: %.2f\n\n',toc(t));

%%% zero-fill data structures if needed
if lvl ~= K
    for newLvl=lvl:K
        FEASIBILITY(newLvl,1:newLvl)={0};
        CENTROIDS(newLvl,1:newLvl)={0};
        MEMBERSHIPS(newLvl,1:newLvl)={0};
        HYPERPLANES(newLvl,1:newLvl)={0};
        COVARIANCES(newLvl,1:newLvl)={0};
        CARDINALITIES(newLvl,1:newLvl)={0};
        MAXANFISMF(newLvl,1:newLvl)={0};
    end
    DBI(lvl:K)=NaN;
    SILHOUETTE(lvl:K)=NaN;
end
% if isempty(r)==false
%     FEASIBILITY = FEASIBILITY(1:r,1:r);
%     CENTROIDS = CENTROIDS(1:r,1:r);
%     MEMBERSHIPS = MEMBERSHIPS(1:r,1:r);
%     HYPERPLANES = HYPERPLANES(1:r,1:r);
%     COVARIANCES = COVARIANCES(1:r,1:r);
%     CARDINALITIES = CARDINALITIES(1:r,1:r);
%     DBI = DBI(1:r);
%     SILHOUETTE = SILHOUETTE(1:r);
%     MAXANFISMF = MAXANFISMF(1:r,1:r);
% end

%%% Output
outputArg = {FEASIBILITY,CENTROIDS,MEMBERSHIPS,HYPERPLANES,COVARIANCES,CARDINALITIES,DBI,SILHOUETTE,MAXANFISMF};
end

%% More functions
function [Ahat, froNorm] = fixCovarianceMatrix(A)
 
% symmetrize A into B
B = (A + A')/2;
 
% Compute the symmetric polar factor of B. Call it H.
% Clearly H is itself SPD.
[~,Sigma,V] = svd(B);
H = V*Sigma*V';
 
% get Ahat in the above formula
Ahat = (B+H)/2;
 
% ensure symmetry
Ahat = (Ahat + Ahat')/2;
 
% test that Ahat is in fact PD. if it is not so, then tweak it just a bit.
p = 1;
k = 0;
while p ~= 0
    [~,p] = chol(Ahat);
    k = k + 1;
    if p ~= 0
        % Ahat failed the chol test. It must have been just a hair off,
        % due to floating point trash, so it is simplest now just to
        % tweak by adding a tiny multiple of an identity matrix.
        mineig = min(eig(Ahat));
        Ahat = Ahat + (-mineig*k.^2 + eps(mineig))*eye(size(A));
    end
    if k>=100
        Ahat = Ahat + k^2*eps*eye(size(Ahat));
    end
end
froNorm = norm(Ahat-A,'fro');

end