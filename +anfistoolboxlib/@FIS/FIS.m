% Copyright (c) 2017, 2018 Stefano Leonori, Alessio Martino, Massimiliano Luzi
% University of Rome "La Sapienza"
%
% stefano.leonori@uniroma1.it, alessio.martino@uniroma1.it, massimiliano.luzi@uniroma1.it
%
% This file is part of AnfisToolbox.
%
% AnfisToolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% AnfisToolbox is distributed in the hope that it will be useful. 
% IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
% INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
% PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
% HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
% CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
% OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AnfisToolbox.If not, see<http://www.gnu.org/licenses/>.

classdef FIS
% Main class of the ANFIS classifier
    properties
        % Antecedent
        mu;
        C;
        % Consequent
        hyperplanes;
        ruleWeights;
        
        %num Rules
        numRules;
        
        % ANFISOptions
        ANFISOptions;
        
        % Training configuration
        trainingOptions;
        k;
        epsilon;
        memberships;
    
        % Performance indeces
        rmse;
        loss;
        DBI;
        silhouette;
    end
    
    methods
        % Constructor
        function obj = FIS(mu, C, hyperplanes, ruleWeights, ANFISOptions, varargin)
			% FIS: Constructor of the ANFIS optimizer
			%
			% Input:
            % mu: centroids of the anfis rules.
            % C: covariances of the anfis rules.
            % hyperplanes: hyperplanes of the anfis rules.
            % ruleWeights: weights of the anfis rules.
            % ANFISOptions: Options of the ANFIS system
			% trainingOptions: Options for the training algorithm
			% varargin: Supplementary options
			%
			% Output:
			% obj: FIS class object
			
			% Import library
            import anfistoolboxlib.*
            
            % Define the input parser
            p = inputParser;
            p.addRequired('mu');
            p.addRequired('C');
            p.addRequired('hyperplanes');
            p.addRequired('ruleWeights');      
            p.addRequired('ANFISOptions')
            p.addParameter('k', []);
            p.addParameter('epsilon', []);
            p.addParameter('memberships', []);
            p.addParameter('DBI', []);
            p.addParameter('silhouette', []);
            p.addParameter('trainingOptions', []);

            % Parse arguments
            p.parse(mu, C, hyperplanes, ruleWeights, ANFISOptions, varargin{:});
            
            % Set anfis parameters
            obj.mu = mu;
            obj.C = C;
            obj.hyperplanes = hyperplanes;
            obj.ruleWeights = ruleWeights;
            obj.numRules = length(mu);
            
            % Anfis options
            obj.ANFISOptions = ANFISOptions;
            
            % Training configuration
            obj.k = p.Results.k;
            obj.epsilon = p.Results.epsilon;
            obj.memberships = p.Results.memberships;
            obj.trainingOptions = p.Results.trainingOptions;
            
            % Performance indeces
            obj.rmse = inf;
            obj.DBI = p.Results.DBI;
            obj.silhouette = p.Results.silhouette;      
        end
    end
end