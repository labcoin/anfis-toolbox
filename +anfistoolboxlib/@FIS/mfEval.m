function [MF_val, MF_id] = mfEval(obj, x)
numSamples = size(x, 1);
numDimensions = size(x, 2);

MF_val = zeros(numSamples,1);
MF_id = zeros(numSamples,1);
MF = zeros(obj.numRules,1);

for k=1:numSamples
    for iterMF=1:obj.numRules
        gamma = 1e-7*eye(numDimensions);
        MF(iterMF) = exp(-0.5*(x(k,:)-obj.mu{iterMF})*((obj.C{iterMF}+gamma)\(x(k,:)-obj.mu{iterMF})'));
    end
    [winner_value,winner_mf]=max(MF);
    MF_val(k) = winner_value;
    MF_id(k) = winner_mf;
end
end