% Copyright (c) 2017, 2018 Stefano Leonori, Alessio Martino, Massimiliano Luzi
% University of Rome "La Sapienza"
%
% stefano.leonori@uniroma1.it, alessio.martino@uniroma1.it, massimiliano.luzi@uniroma1.it
%
% This file is part of AnfisToolbox.
%
% AnfisToolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% AnfisToolbox is distributed in the hope that it will be useful. 
% IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
% INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
% PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
% HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
% CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
% OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AnfisToolbox.If not, see<http://www.gnu.org/licenses/>.+

function y = predict(obj, x)
% FIS::x: Method for predicting the ANFIS output given the input x. The
% best trained fis will be considered.
%
% Input:
% x: input variables    
%
% Output:
% y: predicted output

% Get dimensions
numSamples = size(x,1);
numDimensions = size(x,2);
numMF = obj.numRules;

w = obj.ruleWeights;
y = zeros(numSamples,1);

% Fuzzification
MFval = zeros(numMF,1);
ruleVal=zeros(numMF,1);
for k=1:numSamples
    for iterMF=1:numMF
        gamma = 1e-7*eye(numDimensions);
        if obj.ANFISOptions.sphereFlag
            C = mean(diag(obj.C{iterMF}))*eye(numDimensions);
            MFval(iterMF) = exp(-0.5*(x(k,:)-obj.mu{iterMF})*((C+gamma)\(x(k,:)-obj.mu{iterMF})'));
        else
            MFval(iterMF) = exp(-0.5*(x(k,:)-obj.mu{iterMF})*((obj.C{iterMF}+gamma)\(x(k,:)-obj.mu{iterMF})'));
        end
        ruleVal(iterMF) = [1 x(k,:)]*obj.hyperplanes{iterMF};
    end
    
    % deFuzzification 
    switch obj.ANFISOptions.defuzzification
        case 'WTA'
            [~,winner]=max(MFval.*w);
            y(k) = ruleVal(winner);

        case 'AVG'
            y(k) = sum(MFval.*w.*ruleVal) / ( MFval'*w + 1e-7);   
            
        otherwise
            error('Wrong defuzzification mode. Available "WTA" or "AVG"');
    end

end
end