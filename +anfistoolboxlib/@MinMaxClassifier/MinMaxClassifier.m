% Copyright (c) 2017, 2018 Stefano Leonori, Alessio Martino, Massimiliano Luzi
% University of Rome "La Sapienza"
%
% stefano.leonori@uniroma1.it, alessio.martino@uniroma1.it, massimiliano.luzi@uniroma1.it
%
% This file is part of ClassifierLibrary.
%
% ClassifierLibrary is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% HeuristicLibrary is distributed in the hope that it will be useful. 
% IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
% INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
% PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
% HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
% CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
% OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with ClassifierLibrary.If not, see<http://www.gnu.org/licenses/>.

classdef MinMaxClassifier
% Main class of the ANFIS classifier
    properties
        % MinMaxClassifier
        labels;
        numHyperboxes;
        hyperboxes;
        rotatingMatrices;
        
        lambda;
        gamma;
        trainingMode; 
        parallelFlag;
        architecture;
        timeout;
        
        % Print Flag
        printFlag;
        
        % Verbose
        verboseFlag;
    end
    
    methods
        % Constructor
        function obj = MinMaxClassifier(varargin)
			% MinMaxClassifier: Constructor of the MinMaxClassifier optimizer
			%
			% Input:
			% varargin: Supplementary options
			%
			% Output:
			% obj: ANFIS class object
			
			% Import library
            import anfistoolboxlib.MinMaxClassifier.*
            
% 			% Handle for the minimal number of required arguments
%             if nargin <1
%                 error('The constructor must receive at leas one arguments: 1. The training options');
%             end
            
            % Default algorithm parameters
            default_lambda = 0.2;
            default_gamma = 1;
            default_trainingMode = 'Parc'; %'Parc' 'Arc' 'Gparc'
            default_parallelFlag = false;
            default_architecture = computer('arch');
            default_timeout = 30*60; % 30 minuti
            
            default_verboseFlag = false;
            default_printFlag = false;

            % Define the input parser
            p = inputParser;
            p.addParameter('lambda', default_lambda);
            p.addParameter('gamma', default_gamma);
            p.addParameter('trainingMode', default_trainingMode);
            p.addParameter('parallelFlag', default_parallelFlag);
            p.addParameter('architecture', default_architecture);
            p.addParameter('timeout', default_timeout);
            p.addParameter('verbose', default_verboseFlag);
            p.addParameter('print', default_printFlag);
            
            % Parse arguments
            p.parse(varargin{:});
            
            % Parse Options
            obj.lambda = p.Results.lambda;
            obj.gamma = p.Results.gamma;
            obj.trainingMode = p.Results.trainingMode;  
            obj.parallelFlag = p.Results.parallelFlag; 
            obj.architecture = p.Results.architecture; 
            obj.timeout = p.Results.timeout; 
            
            obj.verboseFlag = p.Results.verbose;
            obj.printFlag = p.Results.print;
        end
    end
end

