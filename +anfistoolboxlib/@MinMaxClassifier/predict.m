function [label, hyperbox_id, mf] = predict(obj, X)
import anfistoolboxlib.MinMaxClassifier.*

% Get dimensions
numberOfPattern=(size(X,1));
numberOfVariables=size(X,2);

mf = zeros(numberOfPattern, obj.numHyperboxes);
for n=1:numberOfPattern
    mf(n,:) = obj.simpson(X(n,:));
end

[~, hyperbox_id] = max(mf, [], 2);
label = cell2mat(obj.labels(hyperbox_id));
end