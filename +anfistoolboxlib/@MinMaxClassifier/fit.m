function obj = fit(obj, X_TR, Y_TR, varargin)
import anfistoolboxlib.MinMaxClassifier.*

% Get dimensions
numberOfLabels = length(Y_TR);
numberOfPattern = (size(X_TR,1));
numberOfVariables = size(X_TR,2);

MinMaxHome = mfilename('fullpath');
MinMaxHome = fileparts(MinMaxHome);

% create temporary directory
tempDir = [MinMaxHome,'\temp'];
[~, ~, ~] = mkdir([MinMaxHome,'\temp']);

% Create univoque id
ID_TrFile = strcat(mat2str(clock),sprintf('%.4f',rand));

% Cleanup for deleting temporary files
cleanupObj = onCleanup(@() CleanupFcn(tempDir, ID_TrFile));

% Get path training file
trainingFile_ct4_fileName = ['dataraw_lambda_',ID_TrFile,'.ct4'];
trainingFile_ct4_fullPath = fullfile(tempDir,trainingFile_ct4_fileName);

% Get path output file
outModel_fileName = ['model_' ID_TrFile, '.txt'];
outModel_fullPath = fullfile(tempDir,outModel_fileName);

obj.mat2ct4(X_TR, Y_TR, trainingFile_ct4_fullPath);

cmd = sprintf('"%s\\bin\\%s\\gparctrain.exe" -a %s -l %.2f -g %.2f -m "%s" -t "%s" -j -d -e -i', ...
     MinMaxHome, obj.architecture, obj.trainingMode, obj.lambda, obj.gamma, outModel_fullPath, trainingFile_ct4_fullPath);
        
minmax_ID = ['minmax_ID_',ID_TrFile];
cmd = sprintf('start "%s" /min %s', minmax_ID, cmd);
[~,~] = system(cmd);
    
watchdog = tic;
watchdog_failure = false;
while isempty(dir(outModel_fullPath))    
    if toc(watchdog) > obj.timeout
        fprintf('Timeout in the minmax classifier \n');
        cmd = sprintf('Taskkill /fi "WINDOWTITLE eq minmax_ID_%s" /im gparctrain.exe /f', ID_TrFile);
        [~,~] = system(cmd);
        watchdog_failure = true;
        delete([tempDir, '\*' ID_TrFile '*']);
        break;
    end
end

pause(1+rand*2)

if ~isempty(dir(outModel_fullPath)) && ~watchdog_failure
    raw = {};
    fid = fopen(outModel_fullPath);
    tline = fgetl(fid);
    raw=[raw {tline}];
    while ischar(tline)
        %     disp(tline)
        tline = fgetl(fid);
        raw=[raw {tline}];
    end
    fclose(fid);
    raw=raw';
    
%     try
        numberOfHyperboxes= str2num(raw{find(cellfun(@(x) ~isempty(strfind(x,'HyperBoxes')),raw))+1});
        find_hyperboxes=find(cellfun(@(x) ~isempty(strfind(x,'HyperBox_')),raw));

    matrice_Hyperboxes=struct('min',[],'max',[],'label',[],'matriceDiRotazione',[] );
    
    flagGparc = strcmp(obj.trainingMode, 'Gparc');
    
    for iter_conversione=1:numberOfHyperboxes
        tmp_min=raw{find_hyperboxes(iter_conversione)+ flagGparc*numberOfVariables+1};
        tmp_min = strsplit(tmp_min,'\t');
        tmp_min=cellfun(@(x) str2num(x),tmp_min);
        
        matrice_Hyperboxes(iter_conversione).min=tmp_min;
        
        tmp_max=raw{find_hyperboxes(iter_conversione) + flagGparc*numberOfVariables +2};
        tmp_max = strsplit(tmp_max,'\t');
        tmp_max=cellfun(@(x) str2num(x),tmp_max);
        
        matrice_Hyperboxes(iter_conversione).max=tmp_max;
        
        tmp_label=raw{find_hyperboxes(iter_conversione)+ flagGparc*numberOfVariables+5};
        matrice_Hyperboxes(iter_conversione).label=str2num(tmp_label);
        
        if flagGparc==true
            tmp_matr_rotazione=zeros(numberOfVariables,numberOfVariables);
            for iter_rotazione=1:numberOfVariables
                tmp_rotaz=raw{find_hyperboxes(iter_conversione)+iter_rotazione};
                tmp_rotaz = strsplit(tmp_rotaz,'\t');
                tmp_rotaz=cellfun(@(x) str2num(x),tmp_rotaz);
                tmp_matr_rotazione(iter_rotazione,:)=tmp_rotaz;
            end
        else
            tmp_matr_rotazione=eye(numberOfVariables,numberOfVariables);
        end
        matrice_Hyperboxes(iter_conversione).matriceDiRotazione=tmp_matr_rotazione;
    end
    
    NUM_HYPERBOXES=size(matrice_Hyperboxes,2);
    RETTANGOLI=cell(NUM_HYPERBOXES,2);
    MATRICIROTAZIONE=cell(NUM_HYPERBOXES,1);
    LABELS=cell(NUM_HYPERBOXES,1);
%         catch
%         a=1;
%     end
    for iterHB=1:NUM_HYPERBOXES
        % Hyerbox
        RETTANGOLI{iterHB,1}= matrice_Hyperboxes(iterHB).min;
        RETTANGOLI{iterHB,2}= matrice_Hyperboxes(iterHB).max;
        
        % Rotating matrix
        MATRICIROTAZIONE(iterHB)={matrice_Hyperboxes(iterHB).matriceDiRotazione'};
        
        % Labels
        LABELS{iterHB} = matrice_Hyperboxes(iterHB).label;
        
        %% DA CAMBIARE !!!
        % ANTIRUOTA X-TR
        tmp_XTR_antiruotato=X_TR*(MATRICIROTAZIONE{iterHB});
        CARDINALITIES{iterHB} = sum(prod(tmp_XTR_antiruotato<matrice_Hyperboxes(iterHB).max,2) & prod(tmp_XTR_antiruotato>matrice_Hyperboxes(iterHB).min,2));
        
    end
    obj.hyperboxes = RETTANGOLI;
    obj.numHyperboxes = NUM_HYPERBOXES;
    obj.rotatingMatrices = MATRICIROTAZIONE;
    obj.labels = LABELS;
else
    obj.hyperboxes = [];
    obj.numHyperboxes = 0;
    obj.rotatingMatrices = [];
    obj.labels = [];
end
end

function CleanupFcn(tempDir, ID_TrFile)
    cmd = sprintf('Taskkill /fi "WINDOWTITLE eq minmax_ID_%s" /im gparctrain.exe /f', ID_TrFile);
    [~,~] = system(cmd);
    delete([tempDir, '\*' ID_TrFile '*']);
end