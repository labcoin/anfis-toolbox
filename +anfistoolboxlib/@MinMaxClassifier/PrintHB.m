function fig = PrintHB(minmaxClassifier, patternSet)

if size(patternSet,2) > 2
    fprintf('Impossible to plot memberships in more than two dimensions\n');
    return
end

fig = figure;
Colore=colormap(hsv(minmaxClassifier.numHyperboxes));

[label, hb_id] = minmaxClassifier.predict(patternSet);
for iterHB = 1:minmaxClassifier.numHyperboxes    
    printpattern = patternSet(hb_id == iterHB,:);
    
    pointsOfNotRotatedRectangle{iterHB,1}= [minmaxClassifier.hyperboxes{iterHB,1}(1) minmaxClassifier.hyperboxes{iterHB,1}(2)];
    pointsOfNotRotatedRectangle{iterHB,2}= [minmaxClassifier.hyperboxes{iterHB,1}(1) minmaxClassifier.hyperboxes{iterHB,2}(2)];
    pointsOfNotRotatedRectangle{iterHB,3}= [minmaxClassifier.hyperboxes{iterHB,2}(1) minmaxClassifier.hyperboxes{iterHB,1}(2)];
    pointsOfNotRotatedRectangle{iterHB,4}= [minmaxClassifier.hyperboxes{iterHB,2}(1) minmaxClassifier.hyperboxes{iterHB,2}(2)];

    for iterCella=1:4
        tmp_matriceRuottata=(cell2mat(pointsOfNotRotatedRectangle(iterHB,iterCella))*minmaxClassifier.rotatingMatrices{iterHB}')  ;
        valid_ROTATEDRECTANGLE(iterHB,iterCella)=mat2cell(tmp_matriceRuottata,1);
    end
    
    ROTATEDRECTANGLE_X1=[valid_ROTATEDRECTANGLE{iterHB,2}(1) valid_ROTATEDRECTANGLE{iterHB,1}(1) valid_ROTATEDRECTANGLE{iterHB,3}(1) valid_ROTATEDRECTANGLE{iterHB,4}(1) valid_ROTATEDRECTANGLE{iterHB,2}(1)];
    ROTATEDRECTANGLE_X2=[valid_ROTATEDRECTANGLE{iterHB,2}(2) valid_ROTATEDRECTANGLE{iterHB,1}(2) valid_ROTATEDRECTANGLE{iterHB,3}(2) valid_ROTATEDRECTANGLE{iterHB,4}(2) valid_ROTATEDRECTANGLE{iterHB,2}(2)];
        
    % STAMPA RETTANGOLO RUOTATO
    plot(printpattern(:,1),printpattern(:,2),'o','Color',Colore(unique(label(hb_id==iterHB)),:)); 
    hold on
    plot(ROTATEDRECTANGLE_X1,ROTATEDRECTANGLE_X2,'-','lineWidth',2,'Color', Colore(unique(label(hb_id==iterHB)),:));

    xlim([0 1]);ylim([0 1]);
    axis square
    
    title('MinMax Hyperboxes')
end

end