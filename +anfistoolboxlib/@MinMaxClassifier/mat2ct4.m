function mat2ct4(obj, M, LABELS, completePath)
%MAT2CT4 Write MATLAB data matrix to ct4
%   --- Input Arguments
%   M: data matrix (size NxF where N is the number of patterns and F is the number of features)
%   LABELS: labels vector (size Nx1)
%   completePath: path + filename for output file
%
%   --- Output Arguments
%   None. Just check the output path.

[numPatterns, numFeatures] = size(M);
fID = fopen(completePath,'w');

% Cleanup for deleting temporary files
cleanupObj = onCleanup(@() fclose(fID));

% write header
fprintf(fID,'Collezione testo versione 4.0\n');
fprintf(fID,'\n');
fprintf(fID,'myDataset\n');
fprintf(fID,'%d\n',numPatterns);
fprintf(fID,'\n');
fprintf(fID,'%d\n',1); % 1 structure
fprintf(fID,'s\t%d\tREAL\n',numFeatures); % s = dummy name of the structure
fprintf(fID,'\n');

% eval 0-based pattern ID
ID = (1:numPatterns);
ID = ID-1;

% write data matrix
for i = 1:numPatterns
    fprintf(fID,'%d\t',ID(i));
    for j=1:numFeatures
        fprintf(fID,'%.6f\t',M(i,j));
    end
    fprintf(fID,'%d\n',LABELS(i));
end

end