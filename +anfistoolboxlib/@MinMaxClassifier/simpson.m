function y = simpson(obj, x)
import anfistoolboxlib.MinMaxClassifier.*

N = size(x,2);
numHyperboxes = length(obj.hyperboxes);

if numHyperboxes > 0
    for iterHB = 1:numHyperboxes
        min_cmp(iterHB,:) = obj.hyperboxes{iterHB,1} - x*obj.rotatingMatrices{iterHB};
        max_cmp(iterHB,:) = x*obj.rotatingMatrices{iterHB} - obj.hyperboxes{iterHB,2};
    end
    
    
    f_min = (obj.gamma*min_cmp).*(obj.gamma*min_cmp>0) + (1-obj.gamma*min_cmp).*(obj.gamma*min_cmp>1);
    f_max = (obj.gamma*max_cmp).*(obj.gamma*max_cmp>0) + (1-obj.gamma*max_cmp).*(obj.gamma*max_cmp>1);
    
    y = sum(1 - f_min - f_max, 2)/N;
else
    y = nan;
end
end