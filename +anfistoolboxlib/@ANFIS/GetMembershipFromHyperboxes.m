% Copyright (c) 2017, 2018 Stefano Leonori, Alessio Martino, Massimiliano Luzi
% University of Rome "La Sapienza"
%
% stefano.leonori@uniroma1.it, alessio.martino@uniroma1.it, massimiliano.luzi@uniroma1.it
%
% This file is part of AnfisToolbox.
%
% AnfisToolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% AnfisToolbox is distributed in the hope that it will be useful. 
% IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
% INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
% PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
% HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
% CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
% OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AnfisToolbox.If not, see<http://www.gnu.org/licenses/>.

function [CENTROIDS_CLASSIFIER, HYPERPLANES_CLASSIFIER, COVARIANCE_CLASSIFIER, CARDINALITIES_CLASSIFIER, valid_NUM_HYPERBOXES]...
            = GetMembershipFromHyperboxes(obj, minmaxClassifier, X_TR, Y_TR)
% ANFIS::GetMembershipFromHyperboxes: Method for retrieving the anfis rules
% from the hyperbox created by the minmax classifier
%
% Input:
% minmaxClassifier: instance of the minmax classifier
% X_TR: Input values of the Training Set
% Y_TR: Output values of the Training Set
%
% Output:
% CENTROIDS_CLASSIFIER: centroids of the anfis rules
% HYPERPLANES_CLASSIFIER: hyperplanes of the anfis rules
% COVARIANCE_CLASSIFIER: covariance matrices of the anfis rules
% CARDINALITIES_CLASSIFIER: cardinalities of the anfis rules
% valid_NUM_HYPERBOXES: number of hyperboxes of the anfis rules

numPatterns = size(X_TR, 1);
numVariables = size(X_TR, 2);

% Get minmax outputs
RECTANGLE = minmaxClassifier.hyperboxes;
MATRIXROTATION = minmaxClassifier.rotatingMatrices;
NUM_HYPERBOXES = minmaxClassifier.numHyperboxes;
HB_CLUSTER_ASSIGNMENT =  minmaxClassifier.labels;

%% Check number of variables
if numVariables ~= size(X_TR,2), error('ERROR PATTERN DEFINITION - NUMBER OF VARIABLES DEFINITION');end

PATTERN_HB_MEMBERSHIP=zeros(numPatterns, NUM_HYPERBOXES); 

for iter_pattern=1:length(PATTERN_HB_MEMBERSHIP)
    tmp_elemento=X_TR(iter_pattern,:);
    for iter_HB=1: NUM_HYPERBOXES       
        tmp_element_antirotated = tmp_elemento*MATRIXROTATION{iter_HB};
        PATTERN_HB_MEMBERSHIP(iter_pattern,iter_HB)=(prod((tmp_element_antirotated+.0)>=RECTANGLE{iter_HB,1})*...
                                prod((tmp_element_antirotated-.0)<=RECTANGLE{iter_HB,2} ));
    end
end

cardinality_HB = sum(PATTERN_HB_MEMBERSHIP);

valid_HyperboxesFlag = cardinality_HB > 1;
valid_HYPERBOXES = RECTANGLE(valid_HyperboxesFlag,:);
valid_MATRIXROTATION = MATRIXROTATION(valid_HyperboxesFlag,:);
valid_HB_CLUSTER_ASSIGNMENT = HB_CLUSTER_ASSIGNMENT(valid_HyperboxesFlag, :);
valid_PATTERN_HB_MEMBERSHIP = PATTERN_HB_MEMBERSHIP(:,valid_HyperboxesFlag);
valid_NUM_HYPERBOXES = sum(valid_HyperboxesFlag);
valid_CARDINALITA_HB = cardinality_HB(valid_HyperboxesFlag);

valid_PATTERN=cell(1,valid_NUM_HYPERBOXES);
valid_PATTERN_Y=cell(1,valid_NUM_HYPERBOXES);
CENTROIDS_CLASSIFIER=cell(1,valid_NUM_HYPERBOXES);
COVARIANCE_CLASSIFIER=cell(1,valid_NUM_HYPERBOXES);
HYPERPLANES_CLASSIFIER=cell(1,valid_NUM_HYPERBOXES);
CARDINALITIES_CLASSIFIER=cell(1,NUM_HYPERBOXES);
for iter_HB=1:valid_NUM_HYPERBOXES
    valid_PatternHBAssociation = valid_PATTERN_HB_MEMBERSHIP(:,iter_HB)==1;
    valid_PATTERN{1,iter_HB}= X_TR(valid_PatternHBAssociation,:);
    valid_PATTERN_Y{1,iter_HB}= Y_TR(valid_PatternHBAssociation,:);
    
    if length(valid_PatternHBAssociation)> numVariables
        CENTROIDS_CLASSIFIER{1,iter_HB}=mean(valid_PATTERN{1,iter_HB},1);
        COVARIANCE_CLASSIFIER{1,iter_HB}=cov(valid_PATTERN{1,iter_HB});
        CARDINALITIES_CLASSIFIER{1, iter_HB} = valid_CARDINALITA_HB(iter_HB);
        HYPERPLANES_CLASSIFIER{iter_HB} = pinv([ones(size(valid_PATTERN{1,iter_HB},1),1) valid_PATTERN{1,iter_HB}]) * valid_PATTERN_Y{1,iter_HB};
    elseif length(valid_PatternHBAssociation)> 1
        CENTROIDS_CLASSIFIER{1,iter_HB}=mean(valid_PATTERN{1,iter_HB},1);
        COVARIANCE_CLASSIFIER{1,iter_HB}= eye(size(valid_PATTERN{1,iter_HB},2)) * mean(pdist2( (valid_PATTERN{1,iter_HB}),CENTROIDS_CLASSIFIER{1,iter_HB}));
        CARDINALITIES_CLASSIFIER{1, iter_HB} = valid_CARDINALITA_HB(iter_HB);
        HYPERPLANES_CLASSIFIER{iter_HB} = [mean(valid_PATTERN_Y{1,iter_HB});  zeros(size(valid_PATTERN{1,iter_HB},1),1) ];
    elseif length(valid_PatternHBAssociation)== 1
        CENTROIDS_CLASSIFIER{1,iter_HB}=mean(valid_PATTERN{1,iter_HB},1);
        COVARIANCE_CLASSIFIER{1,iter_HB}= eye(size(valid_PATTERN{1,iter_HB},2)) * eps;
        CARDINALITIES_CLASSIFIER{1, iter_HB} = valid_CARDINALITA_HB(iter_HB);
        HYPERPLANES_CLASSIFIER{iter_HB} = [mean(valid_PATTERN_Y{1,iter_HB});  zeros(size(valid_PATTERN{1,iter_HB},1),1) ];
    end
end

end

