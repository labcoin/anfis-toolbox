% Copyright (c) 2017, 2018 Stefano Leonori, Alessio Martino, Massimiliano Luzi
% University of Rome "La Sapienza"
%
% stefano.leonori@uniroma1.it, alessio.martino@uniroma1.it, massimiliano.luzi@uniroma1.it
%
% This file is part of AnfisToolbox.
%
% AnfisToolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% AnfisToolbox is distributed in the hope that it will be useful.
% IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
% INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
% PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
% HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
% CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
% OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AnfisToolbox.If not, see<http://www.gnu.org/licenses/>.

function obj = minmax(obj, X_TR, Y_TR, varargin)
% ANFIS::minmax: Method for training the ANFIS toolbox
%
% Input:
% X_TR: Input values of the Training Set
% Y_TR: Output values of the Training Set
% varargin: optional inputs for validation:
%   - 'ValidationData': cell array containing input and output validation
%   data: example {X_VL, Y_VL}
%   - 'ValidationFcn': Handler to a custom validation function. The custom
%   function must respect this definition:
%        bestFisIndex = ValidationFcn(obj, X_VL, Y_VL, validationOptions)
%   where bestFisIndex is the index of the fis_list containing the best resulting fis.
%
% Output:
% obj: instance of the trained ANFIS

%% Import the library
import anfistoolboxlib.*


%% Parse the arguments
% Define the input parser
p = inputParser;
p.addRequired('X_TR');
p.addRequired('Y_TR');
p.addParameter('ValidationData', []);
p.addParameter('ValidationFcn', []);
p.addParameter('ValidationOptions', []);

% Parse arguments
p.parse(X_TR, Y_TR, varargin{:});

% Built Validation Set - If this is not provided, the validation is the
% Training Set itself
validationData = p.Results.ValidationData;
ValidationFcn = p.Results.ValidationFcn;
validationOptions = p.Results.ValidationOptions;

if isempty(validationData)
    X_VL = X_TR;
    Y_VL = Y_TR;
    if isempty(ValidationFcn)
        validationOptions.validationDataFlag = false;
    end
else
    X_VL = p.Results.ValidationData{1};
    Y_VL = p.Results.ValidationData{2};
    
    if isempty(ValidationFcn)
        validationOptions.validationDataFlag = true;
    end
end


%% Normalization
if obj.ANFISOptions.normalizationFlag
    [X_TR, normalizationX] = obj.normalize(X_TR, [], obj.ANFISOptions.normalizationHeadroom);
    [Y_TR, normalizationY] = obj.normalize(Y_TR, [], obj.ANFISOptions.normalizationHeadroom);
    obj.ANFISOptions.normalizationX = normalizationX;
    obj.ANFISOptions.normalizationY = normalizationY;
    
    if validationOptions.validationDataFlag
        X_VL = obj.normalize(X_VL, normalizationX);
        Y_VL = obj.normalize(Y_VL, normalizationY);
    end
end

%% Cleanup for deleting temporary files of minmax
minmaxTempDir = fileparts(mfilename('fullpath'));
idcs = strfind(minmaxTempDir,filesep);
minmaxTempDir = [minmaxTempDir(1:idcs(end)-1),filesep,'@MinMaxClassifier',filesep,'temp',filesep];
cleanupObj = onCleanup(@() delete([minmaxTempDir,'*']));

%% Start Training

parforArg=0;
if obj.trainingOptions.parallelFlag
    parforArg = inf;
end


% create local variables for parfor
MinMaxFcn = @obj.RunMinMax;
if obj.trainedFlag
    sublist=obj.list;
    parfor(iter_k=1:length(sublist), parforArg)
        %for iter_k=1:length(sublist)
        % Run minmax and get fis parameters
        [centroids,...
            covariances,...
            hyperplanes,...
            numHyperboxes] = MinMaxFcn(X_TR, Y_TR, sublist{iter_k}.memberships);
        
        if length(centroids)<=1
            fprintf('Solution unfisieble because there are no HBs\n')
            sublist{iter_k} = [];
        else
            fprintf('Valid HBs = %d \n',numHyperboxes);
            
            ruleWeights = ones(length(centroids), 1);
            sublist{iter_k} = FIS(...
                centroids, ...
                covariances, ...
                hyperplanes, ...
                ruleWeights, ...
                obj.ANFISOptions,   ...
                'k',                sublist{iter_k}.k,                      ....
                'epsilon',          sublist{iter_k}.epsilon,                ...
                'memberships',      sublist{iter_k}.memberships,            ...
                'trainingOptions',  obj.trainingOptions,    ...
                'DBI',              sublist{iter_k}.DBI,           ...
                'silhouette',       sublist{iter_k}.silhouette);
        end
    end
    obj.list = sublist;
    
    obj.list(cellfun(@isempty,obj.list))=[];
    
    %% Validation procedure Save best ANFIS configuration
    if isempty(ValidationFcn)
        validationOptions.alpha = 0.7;
        obj = obj.ValidationFcn(X_VL, Y_VL, validationOptions);
    else
        obj = ValidationFcn(obj, X_VL, Y_VL, validationOptions);
    end
else
    error('The original ANFIS was not trained')
end
end
