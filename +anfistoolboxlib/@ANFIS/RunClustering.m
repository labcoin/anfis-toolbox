% Copyright (c) 2017, 2018 Stefano Leonori, Alessio Martino, Massimiliano Luzi
% University of Rome "La Sapienza"
%
% stefano.leonori@uniroma1.it, alessio.martino@uniroma1.it, massimiliano.luzi@uniroma1.it
%
% This file is part of AnfisToolbox.
%
% AnfisToolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% AnfisToolbox is distributed in the hope that it will be useful. 
% IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
% INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
% PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
% HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
% CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
% OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AnfisToolbox.If not, see<http://www.gnu.org/licenses/>.

function [clust_feasibility, clust_centroids, clust_covariances, clust_memberships, clust_hyperplanes, clust_DBI, clust_silhouette] = RunClustering(obj, X_TR, Y_TR, epsilon)
% ANFIS::RunClustering: Method for executing the clustering algorithm
% needed for training ANFIS.
%
% Input:
% X_TR: Input values of the Training Set
% Y_TR: Output values of the Training Set
% epsilon: weight of the joint space clustering: 1: input space; 0: hyperplane clustering

% Output:
% clust_feasibility: feasibility of the obtained cluestering resuls.
% clust_centroids: centroids of clusters. 
% clust_covariances: covariances of clusters. 
% clust_memberships: membership of each pattern to the clusters.
% clust_hyperplanes: hyperplanes associated to each clusters. 
% clust_cardinalities: cardinalities of clusters. 
% clust_DBI: Davis Boulding Index. 
% clust_silhouette: silhouette index.

% Import library
import anfistoolboxlib.*

% Instantiate Clutering Algorithm
clusteringAlgorithm = Clustering(...
    obj.trainingOptions.clusterMode,        ...
    obj.trainingOptions.representative,     ...
    obj.trainingOptions.distance,           ...
    obj.trainingOptions.numClusters,        ...
    obj.trainingOptions.numReplicates,      ...
    obj.trainingOptions.maxIterations,      ...
    epsilon,            ...
    obj.trainingOptions.davisBouldinFlag,   ...
    obj.trainingOptions.silhoutteFlag,      ...
    obj.trainingOptions.parallelFlag        ...
    );

% Perform clustering
clusteringAlgorithm = clusteringAlgorithm.fit(X_TR, Y_TR);

% Save results
clust_feasibility   = clusteringAlgorithm.feasibility;
clust_centroids     = clusteringAlgorithm.centroids;
clust_memberships   = clusteringAlgorithm.memberships;
clust_hyperplanes   = clusteringAlgorithm.hyperplanes;
clust_covariances   = clusteringAlgorithm.covariances;
clust_cardinalities = clusteringAlgorithm.cardinalities;
clust_DBI           = clusteringAlgorithm.DBI;
clust_silhouette    = clusteringAlgorithm.silhouette;

end

