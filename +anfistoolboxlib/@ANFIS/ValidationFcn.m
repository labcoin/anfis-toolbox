% Copyright (c) 2017, 2018 Stefano Leonori, Alessio Martino, Massimiliano Luzi
% University of Rome "La Sapienza"
%
% stefano.leonori@uniroma1.it, alessio.martino@uniroma1.it, massimiliano.luzi@uniroma1.it
%
% This file is part of AnfisToolbox.
%
% AnfisToolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% AnfisToolbox is distributed in the hope that it will be useful. 
% IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
% INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
% PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
% HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
% CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
% OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AnfisToolbox.If not, see<http://www.gnu.org/licenses/>.

function obj = ValidationFcn(obj, X_VL, Y_VL, validationOptions)
% ANFIS::fit: Method for training the ANFIS toolbox
%
% Input:
% X_VL: Input values of the Validation Set
% Y_VL: Output values of the Validation Set
% validationOptions: struct containing optional variables. It contains at least 
%   - validationDataFlag: for indicating if validation is computed on
%   training set data or validation data has been provided
%   - alpha: weight among rmse and complexity.
% Output:
% bestFisIndex: index of the list containing the best anfis trained ANFIS

bestLoss=inf;
for iter_fis=1:length(obj.list)

    % Run ANFIS
    Y_VL_predicted = obj.list{iter_fis}.predict(X_VL);

    % Evaluate loss
    rmse = sqrt(immse(Y_VL, Y_VL_predicted));

    % Add complexity term
    if ~validationOptions.validationDataFlag
        % Get maximum number of rules among all the ANFISs 
        list = [obj.list{:}];
        maxNumRules = max([list.numRules]);
        loss = rmse*validationOptions.alpha + obj.list{iter_fis}.numRules/maxNumRules*(1-validationOptions.alpha);
    else
        loss = rmse;
    end
    
    obj.list{iter_fis}.rmse = rmse;
    obj.list{iter_fis}.loss = loss;
    
    if loss<bestLoss
        bestLoss=loss;
        bestFisIndex = iter_fis;
    end
end

%% Save best ANFIS configuration
if exist('bestFisIndex', 'var')
    obj.trainedFlag = true;
    obj.fis = obj.list{bestFisIndex};
else
    obj.trainedFlag = false;
    error('The Training algorithm has failed at finding a suitable ANFIS');
end

end