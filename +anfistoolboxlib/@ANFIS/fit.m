% Copyright (c) 2017, 2018 Stefano Leonori, Alessio Martino, Massimiliano Luzi
% University of Rome "La Sapienza"
%
% stefano.leonori@uniroma1.it, alessio.martino@uniroma1.it, massimiliano.luzi@uniroma1.it
%
% This file is part of AnfisToolbox.
%
% AnfisToolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% AnfisToolbox is distributed in the hope that it will be useful. 
% IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
% INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
% PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
% HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
% CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
% OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AnfisToolbox.If not, see<http://www.gnu.org/licenses/>.

function [obj, clust_memberships] = fit(obj, X_TR, Y_TR, varargin)
% ANFIS::fit: Method for training the ANFIS toolbox
%
% Input:
% X_TR: Input values of the Training Set
% Y_TR: Output values of the Training Set
% varargin: optional inputs for validation: 
%   - 'ValidationData': cell array containing input and output validation
%   data: example {X_VL, Y_VL}
%   - 'ValidationFcn': Handler to a custom validation function. The custom
%   function must respect this definition: 
%        bestFisIndex = ValidationFcn(obj, X_VL, Y_VL, validationOptions)
%   where bestFisIndex is the index of the fis_list containing the best resulting fis.    
%
% Output:
% obj: instance of the trained ANFIS
       
%% Import the library
import anfistoolboxlib.*

%% Parse the arguments
% Define the input parser
p = inputParser;
p.addRequired('X_TR');
p.addRequired('Y_TR');
p.addParameter('ValidationData', []);
p.addParameter('ValidationFcn', []);
p.addParameter('ValidationOptions', []);

% Parse arguments
p.parse(X_TR, Y_TR, varargin{:});

% Built Validation Set - If this is not provided, the validation is the
% Training Set itself
validationData = p.Results.ValidationData;
ValidationFcn = p.Results.ValidationFcn;
validationOptions = p.Results.ValidationOptions;

if isempty(validationData)
    X_VL = X_TR;
    Y_VL = Y_TR;
    if isempty(ValidationFcn)
        validationOptions.validationDataFlag = false;
    end
else
    X_VL = p.Results.ValidationData{1};
    Y_VL = p.Results.ValidationData{2};
    
    if isempty(ValidationFcn)
        validationOptions.validationDataFlag = true;
    end
end


%% Normalization
if obj.ANFISOptions.normalizationFlag
    [X_TR, normalizationX] = obj.normalize(X_TR, [], obj.ANFISOptions.normalizationHeadroom);
    [Y_TR, normalizationY] = obj.normalize(Y_TR, [], obj.ANFISOptions.normalizationHeadroom);
    obj.ANFISOptions.normalizationX = normalizationX;
    obj.ANFISOptions.normalizationY = normalizationY;
    
    if validationOptions.validationDataFlag
        X_VL = obj.normalize(X_VL, normalizationX);
        Y_VL = obj.normalize(Y_VL, normalizationY);
    end
end

%% Cleanup for deleting temporary files of minmax
if obj.trainingOptions.minmaxFlag
    minmaxTempDir = fileparts(mfilename('fullpath'));
    idcs = strfind(minmaxTempDir,filesep);
    minmaxTempDir = [minmaxTempDir(1:idcs(end)-1),filesep,'@MinMaxClassifier',filesep,'temp',filesep];
    cleanupObj = onCleanup(@() delete([minmaxTempDir,'*']));
end

%% Start Training
% Set ANFIS as not trained
obj.trainedFlag = false;

parforArg=0;
if obj.trainingOptions.parallelFlag
    parforArg = inf;
end
    
% Start coutner for create the fis_list
for iter_epsilon = 1:length(obj.trainingOptions.epsilon)
    epsilon = obj.trainingOptions.epsilon(iter_epsilon);
    %  Run Clustering algorithm
    fprintf('Running the clustering algorithm with epsilon=%.2f\n\n',epsilon);
    [clust_feasibility, ...
    clust_centroids, ...
    clust_covariances, ...
    clust_memberships, ...
    clust_hyperplanes, ...
    clust_DBI, ...
    clust_silhouette] = obj.RunClustering(X_TR, Y_TR, epsilon);
    
    % create local variables for parfor    
    numK = length(obj.trainingOptions.numClusters);
    numClusters = obj.trainingOptions.numClusters;
    minmaxFlag = obj.trainingOptions.minmaxFlag;
    MinMaxFcn = @obj.RunMinMax;
    sublist = cell(numK,1);
    parfor(iter_k=1:numK, parforArg)
    %for iter_k=1:numK
        % Get k
        k = numClusters(iter_k);
        feasibility = clust_feasibility(k,1:k);
        emptyCells = cellfun(@isempty,feasibility);
        feasibility(emptyCells) = {false};
        feasibleIndices = find(cell2mat(feasibility)==1);
        % Check feasibility
        if isempty(feasibleIndices)
            feasibilitySOL=0;
            fprintf('All the clusters are unfeasible, numOfClusters %d\n',k)
        else
            feasibilitySOL=1;
        end
        
        % Run minmax classifier for feasible solutions only
        if minmaxFlag && feasibilitySOL
            
            % Run minmax and get fis parameters
            [centroids,...
             covariances,...
             hyperplanes,...
             numHyperboxes] = MinMaxFcn(X_TR, Y_TR, clust_memberships(k,1:k));
            
            memberships = clust_memberships(k,1:k);
            if length(centroids)<=1
                feasibilitySOL=false;
                fprintf('Solution unfeasible because there are no HBs\n')
            end
            
            fprintf('Valid HBs = %d \n',numHyperboxes);
            
        else
            % Set fis parameters from clustering only 
            centroids=clust_centroids(k,feasibleIndices);
            covariances=clust_covariances(k,feasibleIndices);
            hyperplanes=clust_hyperplanes(k,feasibleIndices);
            memberships = clust_memberships(k,feasibleIndices);
        end
        
        % Create the fis list
        if feasibilitySOL
            ruleWeights=ones(length(centroids), 1);
            sublist{iter_k} = FIS(...              
                    centroids, ...     
                    covariances, ...
                    hyperplanes, ...
                    ruleWeights, ...
                    obj.ANFISOptions,   ...
                    'k',                k,                      ....
                    'epsilon',          epsilon,                ...
                    'memberships',      memberships,            ...
                    'trainingOptions',  obj.trainingOptions,    ...
                    'DBI',              clust_DBI(k),           ...
                    'silhouette',       clust_silhouette(k));
        end
    end
    for n=1:length(sublist)
        obj.list{(iter_epsilon-1)*numK+n} = sublist{n};
    end
end
obj.list(cellfun(@isempty,obj.list))=[];

%% Validation procedure Save best ANFIS configuration
if isempty(ValidationFcn)
    validationOptions.alpha = 0.7;
    obj = obj.ValidationFcn(X_VL, Y_VL, validationOptions);
else
    obj = ValidationFcn(obj, X_VL, Y_VL, validationOptions);
end

end
