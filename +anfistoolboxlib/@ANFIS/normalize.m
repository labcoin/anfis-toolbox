% Copyright (c) 2017, 2018 Stefano Leonori, Alessio Martino, Massimiliano Luzi
% University of Rome "La Sapienza"
%
% stefano.leonori@uniroma1.it, alessio.martino@uniroma1.it, massimiliano.luzi@uniroma1.it
%
% This file is part of AnfisToolbox.
%
% AnfisToolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% AnfisToolbox is distributed in the hope that it will be useful. 
% IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
% INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
% PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
% HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
% CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
% OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AnfisToolbox.If not, see<http://www.gnu.org/licenses/>.

function [X_norm, normalizationCoefficients] = normalize(obj, X, normalizationCoefficients, headroom)
% FIS::normalize: Method for normalizing the input variables
%
% Input:
% X: vector of inputs
% normalizationCoefficients: [min value, max value]
% headroom: tolerance for the normalization process
% 
% Output:
% X_norm: normalized vector

% If headroom is passed, then re-evaluate the normalization coefficients
if nargin == 3
    X_min = normalizationCoefficients(1,:);
    X_max = normalizationCoefficients(2,:);
elseif nargin==4
    X_min = min(X);
    X_max = max(X);

    delta = headroom*(X_max-X_min);
    X_min = X_min - delta/2;
    X_max = X_max + delta/2;
    
    normalizationCoefficients = [X_min; X_max];
end

X_norm = (X-X_min)./(X_max-X_min);
end