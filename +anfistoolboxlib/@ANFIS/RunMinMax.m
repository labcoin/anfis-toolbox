% Copyright (c) 2017, 2018 Stefano Leonori, Alessio Martino, Massimiliano Luzi
% University of Rome "La Sapienza"
%
% stefano.leonori@uniroma1.it, alessio.martino@uniroma1.it, massimiliano.luzi@uniroma1.it
%
% This file is part of AnfisToolbox.
%
% AnfisToolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% AnfisToolbox is distributed in the hope that it will be useful. 
% IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
% INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
% PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
% HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
% CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
% OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with AnfisToolbox.If not, see<http://www.gnu.org/licenses/>.

function [class_CENTROIDS,class_COVARIANCE,class_HYPERPLANES,class_numHyperboxes] = RunMinMax(obj, X_TR, Y_TR, clust_memberships)
% ANFIS::RunMinMax: Method for executing the minmax algorithm
% used for refining the training of ANFIS.
%
% Input:
% X_TR: Input values of the Training Set
% Y_TR: Output values of the Training Set
% clust_memberships: membership of each pattern to the clusters.

% Output:
% class_CENTROIDS: centroids of clusters. 
% class_COVARIANCE: covariances of clusters. 
% class_HYPERPLANES: hyperplanes associated to each clusters. 
% class_numHyperboxes: number of hyperboxes. 

% Import library
import anfistoolboxlib.*

% Instantiate minmax classifier
minmaxClassifier = MinMaxClassifier(...
    'trainingMode', obj.trainingOptions.minmaxMode, ...
    'lambda', obj.trainingOptions.lambda,           ...
    'timeout', obj.trainingOptions.timeout,           ...
    'print', obj.trainingOptions.printFlag          ...
    );

% Get pattern labels by means of cluster ids
numberOfLabels=length(clust_memberships);

labeledPatterns=[];
for iter_labels=1:numberOfLabels
    numPatternsInCluster = length(clust_memberships{iter_labels});
    labeledPatterns = vertcat(labeledPatterns, [clust_memberships{iter_labels}, iter_labels*ones(numPatternsInCluster,1)]);
end
labeledPatterns=sortrows(labeledPatterns,1);
labels_TR = labeledPatterns(:,2);

% ALESSIO HACK
X_TR = X_TR(labeledPatterns(:,1),:);
Y_TR = Y_TR(labeledPatterns(:,1));

% Run MinMax
minmaxClassifier = minmaxClassifier.fit(X_TR, labels_TR);

fprintf('Number of HBs = %d \n',minmaxClassifier.numHyperboxes);

% Get membership function parameters from hyperboxes
[class_CENTROIDS,class_HYPERPLANES,class_COVARIANCE,class_CARDINALITIES,class_numHyperboxes] = ...
    obj.GetMembershipFromHyperboxes(minmaxClassifier, X_TR, Y_TR);

if obj.trainingOptions.printFlag
    %obj.PrintHB(minmaxClassifier, X_TR);
    PrintHB(minmaxClassifier, X_TR)
end
end

