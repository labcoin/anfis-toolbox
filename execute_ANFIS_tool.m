function [anfis] = execute_ANFIS_tool(testSetting,TrainingSet,TrainingLabels,ValidationSet,ValidationLabels)
%% EXECUTE ANFIS TOOLBOX

if strcmp(testSetting.MinMaxMode,'none')
    testSetting.MinMaxMode=false;
end
% Import library
import anfistoolboxlib.*
% Set ANFIS options
ANFISOptions = struct(...
    'sphereFlag',           testSetting.SPHEREFLAG,   ...
    'defuzzification',      testSetting.DEFUZZIFICATION,  ... 'WTA' or 'AVG'
    'normalizationFlag',    false,  ...
    'ruleWeights',          []      ...
    );
% Set training Options
trainingOptions = struct(...
    'parallelFlag',     testSetting.parallelFlag ,   ... Set the activation or the deactivation of the parallel computing
    ... Clustering
    'clusterMode',      testSetting.CLUSTERING     , ... Set the clustering mode: 'partitional', 'agglomerative', 'divisive'
    'representative',   testSetting.REPRESENTATIVE , ... Set the representative type: 'centroid', 'medoid', 'median'
    'distance',         testSetting.DISTANCE,        ... Set the distance to be used in clustering algorithm:
    ...'euclidean', 'sqEuclidean', 'manhattan', 'mahalanobis' or function handle
    'numClusters',      testSetting.CLUSTER_ARRAY,   ... K candidates. Array ok K value to be tested. The best K will be kept.
    'numReplicates',    30,                          ... Set the number of replications of the clustering algorithm.
    ... The clustering will be run for 'numReplicates' times with a
    ... different intialization. The best solution will be kept.
    'maxIterations',    testSetting.ITERATION,       ... Set the maximum number of itertions of the clusterin algorithm.
    'epsilon',          testSetting.EPSILON,         ... Set the weight of the joint space clustering:
    ... 1: input space; 0: hyperplane clustering
    'davisBouldinFlag', false,                       ... Set the evaluation or not of the Davis Bouldin index
    ... for the performance of the clustering algorithm
    'silhoutteFlag',    false,                       ... Set the evaluation or not of the Silhouette index
    ... for the performance of the clustering algorithm
    'minmaxFlag',       testSetting.MinMax,          ... Activation of the minmax classifier for refining the clustering results
    'minmaxMode',       testSetting.MinMaxMode,      ... 'Parc','Arc','Gparc'
    'lambda',           testSetting.LAMBDA,          ... Tradeoff between accuracy and complexity for the minmax algorithm
    'timeout',          testSetting.TIMEOUT,         ... Timeout of minmax in seconds
    'printFlag',        testSetting.printFlag        ... Flag for activating the print of the hyperboxes
    );
% Instantiate ANFIS
anfis = ANFIS('ANFISOptions', ANFISOptions, 'trainingOptions', trainingOptions);
% Train ANFIS
tic;
anfis =anfis.fit(TrainingSet, TrainingLabels, 'ValidationData', {ValidationSet, ValidationLabels});

end

