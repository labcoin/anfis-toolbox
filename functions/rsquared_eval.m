function [Rsquared] = rsquared_eval(Labels,Labels_predicted)

total = 0; sump = 0; sumt = 0; sumpp = 0; sumtt = 0; sumpt = 0;
for i = 1:length(Labels)
    sump = sump + Labels_predicted(i);
    sumt = sumt + Labels(i);
    sumpp = sumpp + Labels_predicted(i)*Labels_predicted(i);
    sumtt = sumtt + Labels(i)*Labels(i);
    sumpt = sumpt + Labels_predicted(i)*Labels(i);
    total = total + 1;
end
Rsquared = ((total*sumpt-sump*sumt)*(total*sumpt-sump*sumt))/((total*sumpp-sump*sump)*(total*sumtt-sumt*sumt));

end

