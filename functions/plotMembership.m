%% PLOT
function y = plotData(anfis, x) 

import anfistoolboxlib.*

Colore=colormap(hsv(max(anfis.fis.numRules)));
Colore = Colore(randperm(size(Colore,1)),:);

% Print ANFIS membership functions
figure
x1 = 0:.02:1; x2 = 0:.02:1;
[X1,X2] = meshgrid(x1,x2);
[mfValue, mf_id] = anfis.fis.mfEval(x);
for iterMF = 1:anfis.fis.numRules
    printpattern = x(mf_id==iterMF,:);
    
    plot(printpattern(:,1),printpattern(:,2),'o','Color',Colore(iterMF,:));
    hold on
    % cluster
    mu = anfis.fis.mu{iterMF};
    C = anfis.fis.C{iterMF};
    
    plot(mu(1),mu(2), 'p', 'MarkerFaceColor', Colore(iterMF,:) , 'MarkerEdgeColor', 'k', 'MarkerSize', 10);
    
    dC = decomposition(C);
    tf = isIllConditioned(dC);
    if tf==true
        C=C+10e-3;
    end
    F = mvnpdf([X1(:) X2(:)],mu,C);
    F = reshape(F,length(x2),length(x1));
    contour(x1,x2,F,5, 'LineWidth', 1.5, 'Color', Colore(iterMF,:));
    
    title('ANFIS Rules')
    xlim([0 1]);ylim([0 1]);
    axis square
end