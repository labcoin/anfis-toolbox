clear ; close all; clc;

list_folder_results=horzcat('testResults');
list_folder_dataset=horzcat('toyDatasets');
addpath(list_folder_dataset);
addpath(list_folder_results);

list_dataset={'jain'};
%'microgridFastCharge',...
%'housing',...
% 'Aggregation',...
% 'Compound',...
% 'D_31',...
% 'flame',...
% 'jain',...
% 'Lsun',...
% 'pathbased',...
% 'R_15',...
% 'spiral',...
% 'microgridFastCharge',...                    
                
%% Check DATASET 
for dataset=1: length( list_dataset)
    load(horzcat(list_dataset{dataset},'.mat'));
    if (exist('TestLabels') && exist('TrainingLabels') && exist('ValidationLabels')...
        && exist('TestSet') && exist('TrainingSet')    && exist('ValidationSet'))==false
        error('error on dataset to load')
    end
    clearvars -except list_*
end
                
%%  TEST Setting
list_kmeans={'partitional'}; % {'divisive' 'partitional','agglomerative'}
list_clustering_metric={'sqEuclidean'};% {'sqEuclidean','mahalanobis','manhattan'}
list_clustering_representative={'centroid'};% {'centroid','median','medoid'}
list_classifier={'Parc'}; % {'Parc', 'none', 'Gparc'}
list_sphereflag={false};  % generate spherical clusters
list_epsilon={0.0}; % from 0 to 1
list_numberOfReplicate=30; 
list_numberOfietaration=1000;
list_numberOfClustersInterval=[3,3]; 
list_lambda=0; 
list_defuzzification='WTA'; % 'WTA' or 'AVG'  
iter_list=0;

for classifier=1:length(list_classifier)
    for dataset=1:length( list_dataset)
        for iter_kmeans=1:length(list_kmeans)
            for iter_metrics=1:length(list_clustering_metric)
                for representative=1:length(list_clustering_representative)
                    for iter_epsilon=1:length(list_epsilon)
                        for iter_sphereflag=1:length(list_sphereflag)
                            
                            %% LOAD THE DATASET
                            load(horzcat(list_dataset{dataset},'.mat'));
                            
                            %% PARAMETER SETTING FOR EACH TEST
                            iter_list=iter_list+1;
                            
                            TestList(iter_list).sphereflag = list_sphereflag{iter_sphereflag};
                            TestList(iter_list).nameDataset =list_dataset{dataset};
                            TestList(iter_list).numberOfInput = size(TrainingSet,2);
                            TestList(iter_list).numberOfPattern = size(TrainingSet,1);
                            TestList(iter_list).kmeans =       list_kmeans{iter_kmeans};
                            TestList(iter_list).clustering_metric = list_clustering_metric{iter_metrics};
                            TestList(iter_list).clustering_representative = list_clustering_representative{representative};
                            TestList(iter_list).classifier = list_classifier{classifier};
                            TestList(iter_list).epsilon = list_epsilon{iter_epsilon};
                            TestList(iter_list).numberOfReplicates = list_numberOfReplicate;
                            TestList(iter_list).numberOfIteration = list_numberOfietaration;
                            TestList(iter_list).lambda = list_lambda;
                            TestList(iter_list).defuzzification = list_defuzzification;
                            TestList(iter_list).clustering_interval =...
                                    min(list_numberOfClustersInterval(1),...
                                        floor(length(TrainingLabels)/(size(TrainingSet,2)+5)))...
                                        :1:...
                                    min(list_numberOfClustersInterval(2),...
                                        floor(length(TrainingLabels)/(size(TrainingSet,2)+5)));
                            
                            % ID GENERATION
                            TestList(iter_list).ID =...
                                horzcat(...
                                TestList(iter_list).nameDataset(1:4),'_',...
                                TestList(iter_list).kmeans(1:4),'_',...
                                TestList(iter_list).clustering_metric(1:4),'_',...
                                TestList(iter_list).clustering_representative(1:4),'_',...
                                TestList(iter_list).classifier(1:4),'_',...
                                'eps',num2str(TestList(iter_list).epsilon),...
                                'sph',num2str(TestList(iter_list).sphereflag)...
                                );
                            fprintf(TestList(iter_list).ID);fprintf('  \n')
                            TestList(iter_list).PERFORMANCE={};
                        end
                    end
                end
            end
        end
    end
end

pathFile_test=fullfile(horzcat(filesep,list_folder_results),'TestList.mat');
save([pwd pathFile_test ],'TestList');

clearvars -except TestList

execute_test