# ANFIS Toolbox Lib

![](https://img.shields.io/badge/Platform-Windows-red)    ![](https://img.shields.io/badge/Language-Matlab-green)   ![](https://img.shields.io/badge/License-CC4.0-blue)

This repository contains the software developed for the tests published in the paper "A Generalized Framework for ANFIS Synthesis Procedures by Clustering Techniques".

The software proposes a tool for the generation of an Adaptive Neuro Fuzzy Inference System (ANFIS) by clustering techniques for function approximation.

The tool is written in Matlab through Object-Oriented Programming. It allows to easily customize and personalize most of the options to set for the ANFIS synthesis.

Specifically, it is possible to chose the clustering algorithm, the dissimilarity measure, the cluster representative and define the clustering space that can vary from the simple input space to the pure hyperplane space.
The tool is also equipped with different Min-Max classifiers which would run downstream the clustering phase in order to properly refine the ANFIS Membership Functions (MFs).

If you use this software for scientific purposes, please consider citing [our paper](https://doi.org/10.1016/j.asoc.2020.106622):
```
@article{LEONORI2020106622,
title = "A generalized framework for ANFIS synthesis procedures by clustering techniques",
journal = "Applied Soft Computing",
volume = "96",
pages = "106622",
year = "2020",
issn = "1568-4946",
doi = "https://doi.org/10.1016/j.asoc.2020.106622",
url = "http://www.sciencedirect.com/science/article/pii/S1568494620305603",
author = "Leonori, Stefano and Martino, Alessio and Luzi, Massimiliano and Frattale Mascioli, Fabio Massimo and Rizzi, Antonello",
}
```

**Note:** a detailed user guide is under construction.

**Note:** so far, the Min-Max utility works on Windows only. Unix porting is under construction.
